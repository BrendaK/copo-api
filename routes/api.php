<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\MotoController;
use App\Http\Controllers\ScooterController;
use App\Http\Controllers\Moteur2tController;
use App\Http\Controllers\Moteur4tController;
use App\Http\Controllers\Moteur4tCommercialeController;
use App\Http\Controllers\Moteur4tFtController;
use App\Http\Controllers\MoteurEnduroController;
use App\Http\Controllers\WaverunnerController;
use App\Http\Controllers\SavController;
use App\Http\Controllers\ContactController;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::post('login', [UserController::class, 'login']);
Route::post('contact', [ContactController::class, 'create']);

// Route::group(['middleware' => 'auth:api'], function() {

    Route::get('users', [UserController::class, 'getAll']);
    Route::get('users/{id}', [UserController::class, 'getById']);
    Route::post('users', [UserController::class, 'create']);
    Route::post('users/{id}', [UserController::class, 'update']);
    Route::delete('users/{id}', [UserController::class, 'delete']);

    Route::get('motos', [MotoController::class, 'getAll']);
    Route::get('motos/{id}', [MotoController::class, 'getById']);
    Route::post('motos', [MotoController::class, 'create']);
    Route::post('motos/{id}', [MotoController::class, 'update']);
    Route::delete('motos/{id}', [MotoController::class, 'delete']);

    Route::get('scooters', [ScooterController::class, 'getAll']);
    Route::get('scooters/{id}', [ScooterController::class, 'getById']);
    Route::post('scooters', [ScooterController::class, 'create']);
    Route::post('scooters/{id}', [ScooterController::class, 'update']);
    Route::delete('scooters/{id}', [ScooterController::class, 'delete']);

    Route::get('moteur2ts', [Moteur2tController::class, 'getAll']);
    Route::get('moteur2ts/{id}', [Moteur2tController::class, 'getById']);
    Route::post('moteur2ts', [Moteur2tController::class, 'create']);
    Route::post('moteur2ts/{id}', [Moteur2tController::class, 'update']);
    Route::delete('moteur2ts/{id}', [Moteur2tController::class, 'delete']);

    Route::get('moteur4ts', [Moteur4tController::class, 'getAll']);
    Route::get('moteur4ts/{id}', [Moteur4tController::class, 'getById']);
    Route::post('moteur4ts', [Moteur4tController::class, 'create']);
    Route::post('moteur4ts/{id}', [Moteur4tController::class, 'update']);
    Route::delete('moteur4ts/{id}', [Moteur4tController::class, 'delete']);
    
    Route::get('moteur4tFts', [Moteur4tFtController::class, 'getAll']);
    Route::get('moteur4tFts/{id}', [Moteur4tFtController::class, 'getById']);
    Route::post('moteur4tFts', [Moteur4tFtController::class, 'create']);
    Route::post('moteur4tFts/{id}', [Moteur4tFtController::class, 'update']);
    Route::delete('moteur4tFts/{id}', [Moteur4tFtController::class, 'delete']);

    Route::get('moteur4tCommerciales', [Moteur4tCommercialeController::class, 'getAll']);
    Route::get('moteur4tCommerciales/{id}', [Moteur4tCommercialeController::class, 'getById']);
    Route::post('moteur4tCommerciales', [Moteur4tCommercialeController::class, 'create']);
    Route::post('moteur4tCommerciales/{id}', [Moteur4tCommercialeController::class, 'update']);
    Route::delete('moteur4tCommerciales/{id}', [Moteur4tCommercialeController::class, 'delete']);

    Route::get('moteurEnduros', [MoteurEnduroController::class, 'getAll']);
    Route::get('moteurEnduros/{id}', [MoteurEnduroController::class, 'getById']);
    Route::post('moteurEnduros', [MoteurEnduroController::class, 'create']);
    Route::post('moteurEnduros/{id}', [MoteurEnduroController::class, 'update']);
    Route::delete('moteurEnduros/{id}', [MoteurEnduroController::class, 'delete']);

    Route::get('waverunners', [WaverunnerController::class, 'getAll']);
    Route::get('waverunners/{id}', [WaverunnerController::class, 'getById']);
    Route::post('waverunners', [WaverunnerController::class, 'create']);
    Route::post('waverunners/{id}', [WaverunnerController::class, 'update']);
    Route::delete('waverunners/{id}', [WaverunnerController::class, 'delete']);

    Route::get('sav', [SavController::class, 'getAll']);
    
// });
Route::get('sav/{id}', [SavController::class, 'getById']);