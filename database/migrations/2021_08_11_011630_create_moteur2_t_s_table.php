<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMoteur2TSTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('moteur2_t_s', function (Blueprint $table) {
            $table->id();
            $table->integer('product_type_id');
            $table->string('name');
            $table->string('image');
            $table->string('titre');
            $table->string('titre_description');
            $table->longtext('description');
            $table->string('prix');
            $table->longtext('caracteristique_1');
            $table->longtext('caracteristique_2');
            $table->longtext('caracteristique_3');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('moteur2_t_s');
    }
}
