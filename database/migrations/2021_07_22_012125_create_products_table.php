<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('vidéo')->nullable();
            $table->string('image');
            $table->string('titre')->nullable();
            $table->string('petit_titre')->nullable();
            $table->longtext('description')->nullable();
            $table->string('prix');
            $table->string('image_force1')->nullable();
            $table->longtext('force1')->nullable();
            $table->string('image_force2')->nullable();
            $table->longtext('force2')->nullable();
            $table->string('image_force3')->nullable();
            $table->longtext('force3')->nullable();
            $table->string('image_force4')->nullable();
            $table->longtext('force4')->nullable();
            $table->string('image_force5')->nullable();
            $table->longtext('force5')->nullable();
            $table->longtext('caractéristique')->nullable();
            $table->integer('product_type_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
