<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Moteur2T;

class Moteur2TSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $moteur2temps = [
            [
            'product_type_id' => 2,
            'name' => '2DMHS',
            'image' => 'https://dl.airtable.com/.attachmentThumbnails/8769339e52584a917fa670a354622e94/20aaa60d',
            'titre' => 'Le classique',
            'titre_description' => 'Petit mais puissant',
            'description' => 'La plus petite puissance de la gamme Yamaha. Léger, compact, il s’installe facilement et se pilote sans permis. Il conviendra aux petites embarcations pour une utilisation dans les lagons.',
            'prix' => '120 000 XPF TTC',
            'caracteristique_1' => 
                '<li>Type : 2 temps, monocylindre à carburateur 
                </li><li>Cylindrée : 50cc
                </li><li>Bougie : B7HS, BR7HS
                </li><li>Alésage X Course : 42.0X36mm
                </li><li>Taux de compression : 8.3 :1
                </li><li>Plage de régime à plein gaz
                </li><li>Puissance maximale : 2cv
                </li><li>Système d’alimentation : 1 carburateur
                </li><li>Lubrification : Mélange 50 :1
                </li><li>Allumage : Manuel
                </li><li>Mise en route : CDI
                </li><li>Consommation de carburant : 1,4L/h @5000tr/minute</li>',
            'caracteristique_2' => 
                '<li>Poids avec hélice : 10kg
                </li><li>Longueur d’arbre : Court (419mm)
                </li><li>Capacité réservoir d’essence : 1,1L
                </li><li>Capacité réservoir d’huile : 10L</li>',
            'caracteristique_3' => 
                '<li>TRIM : 4 positions, Manuel
                </li><li>Angle de virage : 360 degrés
                </li><li>Hélice : incluse</li>',
            ],
            [
            'product_type_id' => 2,
            'name' => '4CMHS/L',
            'image' => 'https://dl.airtable.com/.attachmentThumbnails/cab13873fb893890be706b804eb54961/7c3e29c2',
            'titre' => 'Le classique',
            'titre_description' => 'Praticité et robustesse',
            'description' => 'Idéal pour les annexes et petites embarcations, le 4cv de Yamaha est robuste et compact. Simple d’entretien et d’utilisation, le permis côtier n’est pas nécessaire pour cette puissance.',
            'prix' => '160 000 XPF TTC',
            'caracteristique_1' => 
                '<li>Type : 2 temps, monocylindre à carburateur 
                </li><li>Cylindrée : 103cc
                </li><li>Bougie : B7HS
                </li><li>Alésage X Course : 54.0X45mm
                </li><li>Taux de compression : 6.5 :1
                </li><li>Plage de régime à plein gaz : 5500 tr/min
                </li><li>Puissance maximale : 4cv
                </li><li>Système d’alimentation : carburateur
                </li><li>Lubrification : Mélange 50 :1
                </li><li>Allumage : Manuel
                </li><li>Mise en route : CDI
                </li><li>Consommation de carburant : 2,8L/h @5500tr/minute</li>',
            'caracteristique_2' => 
                '<li>Poids avec hélice : 21kg, 22kg
                </li><li>Longueur d’arbre : Court (444mm), Long (571mm)
                </li><li>Capacité réservoir d’essence : 2,8L
                </li><li>Capacité réservoir d’huile : 10L</li>',
            'caracteristique_3' => 
                '<li>TRIM : 5 positions, Manuel
                </li><li>Angle de virage : 360 degrés
                </li><li>Equipements inclus : Hélice et coupe circuit</li>',
            ],
            [
            'product_type_id' => 2,
            'name' => '9,9FMHS/L',
            'image' => 'https://dl.airtable.com/.attachmentThumbnails/cb2fb5a09a4c58ecf47867678748ba0e/3015a5e6',
            'titre' => 'Le classique',
            'titre_description' => 'Quand transportabilité va de paire avec performance',
            'description' => 'La réputation du 9.9F n’est plus à refaire. Alliant performance et fiabilité, il est adapté aux annexes et dinghy, mais aussi pour les petites embarcations lagonaires.
            Facile d’entretien, abordable et peu gourmand en carburant, c’est l’un des best seller de la gamme.',
            'prix' => '280 000 XPF TTC',
            'caracteristique_1' => 
                '<li>Type : 2 temps, monocylindre à carburateur 
                </li><li>Cylindrée : 246cc
                </li><li>Bougie : B8HS-10
                </li><li>Alésage X Course : 56.0X50mm
                </li><li>Taux de compression : 7.0 :1
                </li><li>Plage de régime à plein gaz : 5500 tr/min
                </li><li>Puissance maximale : 9,9cv
                </li><li>Système d’alimentation : carburateur
                </li><li>Lubrification : Mélange 50 :1
                </li><li>Allumage : Manuel
                </li><li>Mise en route : CDI
                </li><li>Consommation de carburant : 6,9L/h @5500tr/minute</li>',
            'caracteristique_2' => 
                '<li>Poids avec hélice : 21kg, 22kg
                </li><li>Longueur d’arbre : Court (444mm), Long (571mm)
                </li><li>Capacité réservoir d’huile : 10L</li>',
            'caracteristique_3' => 
                '<li>TRIM : 4 positions, Manuel
                </li><li>Angle de virage : 40 degrés dans chaque direction
                </li><li>Equipements inclus : Hélice, nourrice essence et coupe circuit</li>',
            ],
            [
            'product_type_id' => 2,
            'name' => '15FMHS/L',
            'image' => 'https://dl.airtable.com/.attachmentThumbnails/8a7b6771313b89a75c5af9e6ee96ea92/0989e751',
            'titre' => 'Le classique',
            'titre_description' => 'Fiabilité et puissance',
            'description' => 'Avec une réputation fondée sur des années de performances fiables et puissantes, le 15F à deux temps de Yamaha est un moteur hors du commun dans la gamme Yamaha. Léger, compact et puissant, le 15F de Yamaha est l’option d’équipement pour les petits bateaux en aluminium, les semi-rigides et les annexes. Le 15F est un moteur conçu pour tenir la distance.',
            'prix' => '300 000 XPF TTC',
            'caracteristique_1' => 
                '<li>Type : 2 temps, 2 cylindres en ligne
                </li><li>Cylindrée : 246cc
                </li><li>Bougie : B7HS-10
                </li><li>Alésage X Course : 56.0X50.0mm
                </li><li>Taux de compression : 6.8 :1
                </li><li>Plage de régime à plein gaz : 5500 tr/min
                </li><li>Puissance maximale : 15cv
                </li><li>Système d’alimentation : 1 carburateur
                </li><li>Lubrification : Mélange 50 :1
                </li><li>Allumage : Manuel
                </li><li>Mise en route : CDI
                </li><li>Consommation de carburant : 7,3L/h @5500tr/minute</li>',
            'caracteristique_2' => 
                '<li>Poids avec hélice : 36kg, 38kg
                </li><li>Longueur d’arbre : Court (440mm), Long (567mm)
                </li><li>Capacité réservoir d’huile : 10L</li>',
            'caracteristique_3' => 
                '<li>TRIM : 4 positions, Manuel
                </li><li>Angle de virage : 45 degrés dans chaque direction
                </li><li>Equipements inclus : Hélice, nourrice et coupe circuit</li>',
            ],
            [
            'product_type_id' => 2,
            'name' => '30HMHS/L',
            'image' => 'https://dl.airtable.com/.attachmentThumbnails/60afd039aa1081769dfe678208ab84c6/8beeba00',
            'titre' => 'Le classique',
            'titre_description' => 'Puissant pour une basse consommation',
            'description' => 'Construit autour d’une architecture qui a fait ses preuves, le 30cv de Yamaha fournit une puissance optimale et une consommation de carburant basse. 50 années de recherches et de développement acquises sur le terrain attestent de la qualité de ce moteur.',
            'prix' => '400 000 XPF TTC',
            'caracteristique_1' => 
                '<li>Type : 2 temps, 2 cylindres en ligne
                </li><li>Cylindrée : 496cc
                </li><li>Bougie : B8HS-10
                </li><li>Alésage X Course : 72.0X61.0mm
                </li><li>Taux de compression : 7.0:1
                </li><li>Plage de régime à plein gaz : 5500 tr/min
                </li><li>Puissance maximale : 30cv
                </li><li>Système d’alimentation : 1 carburateur
                </li><li>Lubrification : Mélange 50 :1
                </li><li>Allumage : Manuel
                </li><li>Mise en route : CDI
                </li><li>Consommation de carburant : 12,0L/h @5500tr/minute</li>',
            'caracteristique_2' => 
                '<li>Poids avec hélice : 53kg, 58kg
                </li><li>Longueur d’arbre : Court (423mm), Long (550mm)
                </li><li>Capacité réservoir d’huile : 10L</li>',
            'caracteristique_3' => 
                '<li>TRIM : 4 positions, Manuel
                </li><li>Angle de virage : 40 degrés dans chaque direction
                </li><li>Equipements inclus : Hélice, nourrice et coupe circuit</li>',
            ],
            [
            'product_type_id' => 2,
            'name' => '40XWTL',
            'image' => 'https://dl.airtable.com/.attachmentThumbnails/589eb0875b58be78ac41543ccdc00df8/8f4003e5',
            'titre' => 'Le classique',
            'titre_description' => 'Un moteur fiable',
            'description' => 'Par ses caracteristiques techniques et ses performances, ce modèle est devenu l’un des plus commercialisés sur le segment des 2 temps.
            La fiabilité de ses composants et sa conception compacte en font un produit complet. Il a également le trim électrique, un démarrage manuel et électrique.',
            'prix' => '650 000 XPF TTC',
            'caracteristique_1' => 
                '<li>Type : 2 temps, 2 cylindres en ligne
                </li><li>Cylindrée : 703cc
                </li><li>Bougie : B7HS
                </li><li>Alésage X Course : 80.0X70.0mm
                </li><li>Taux de compression : 6.0 :1
                </li><li>Plage de régime à plein gaz : 5500 tr/min
                </li><li>Puissance maximale : 40cv
                </li><li>Système d’alimentation : 1 carburateur
                </li><li>Lubrification : Mélange 50 :1
                </li><li>Allumage : Electrique et manuel
                </li><li>Mise en route : CDI
                </li><li>Consommation de carburant : 20,0L/h @5500tr/minute</li>',
            'caracteristique_2' => 
                '<li>Poids avec hélice : 81kg
                </li><li>Longueur d’arbre : Long (550mm)
                </li><li>Capacité réservoir d’huile : 10L</li>',
            'caracteristique_3' => 
                '<li>TRIM : Electrique
                </li><li>Angle de virage : 45 degrés dans chaque direction
                </li><li>Equipements inclus : Hélice, nourrice, manette latérale, faisceaux, et coupe circuit</li>',
            ],
            [
            'product_type_id' => 2,
            'name' => '60FETL',
            'image' => 'https://dl.airtable.com/.attachmentThumbnails/6dc9f771f10314c4715abfe2a37109b5/ee50943b',
            'titre' => 'Le classique',
            'titre_description' => 'Performances et polyvalence pour naviguer sereinement',
            'description' => 'Le 60FET est un concentré de performances et de polyvalence. Il permet de naviguer en toute sérénité dans les lagons et près des côtes au large.
            Qu’il s’agisse de pêcher ou transporter du coprah dans les îles, ce moteur a fait ses preuves depuis plus de 10 années de présence sur le territoire',
            'prix' => '795 000 XPF TTC',
            'caracteristique_1' => 
                '<li>Type : 2 temps, 3 cylindres en ligne
                </li><li>Cylindrée : 849cc
                </li><li>Bougie : B8HS-10
                </li><li>Alésage X Course : 72.0X69.5mm
                </li><li>Taux de compression : 6.1 :1
                </li><li>Plage de régime à plein gaz : 5500 tr/min
                </li><li>Puissance maximale : 60cv
                </li><li>Système d’alimentation : 3 carburateurs
                </li><li>Lubrification : Mélange 50 :1
                </li><li>Allumage : Electrique
                </li><li>Mise en route : CDI
                </li><li>Consommation de carburant : 23,0L/h @5500tr/minute</li>',
            'caracteristique_2' => 
                '<li>Poids avec hélice : 106kg
                </li><li>Longueur d’arbre : Long (521mm)
                </li><li>Capacité réservoir d’huile : 10L</li>',
            'caracteristique_3' => 
                '<li>TRIM : Electrique
                </li><li>Angle de virage : 35 degrés dans chaque direction
                </li><li>Equipements inclus : Hélice, nourrice, manette latérale, faisceaux, et coupe circuit</li>',
            ],
            [
            'product_type_id' => 2,
            'name' => '85AET',
            'image' => 'https://dl.airtable.com/.attachmentThumbnails/8c2d9952bd0bcb61e36e90d4662daabf/66abe3c6',
            'titre' => 'Le classique',
            'titre_description' => 'Fiabilité, performance et robustesse',
            'description' => 'Depuis sa sortie en 1978, bien des technologies ont été dérivées et développées à partir de ce moteur légendaire. Son bloc monteur est resté presque inchangé depuis sa création tellement il est fiable, performant et robuste. Utilisé aussi bien sur des poti marara que sur des coques « classiques » il sait s’adapter à l’utilisation que vous en faites. Le 85AET est bel et bien une référence phare de la gamme Yamaha depuis ses débuts',
            'prix' => '960 000 XPF TTC',
            'caracteristique_1' => 
                '<li>Type : 2 temps, 3 cylindres en ligne
                </li><li>Cylindrée : 1140cc
                </li><li>Bougie : B8HS-10
                </li><li>Alésage X Course : 82.0X72.0mm
                </li><li>Taux de compression : 5.1 :1
                </li><li>Plage de régime à plein gaz : 5500 tr/min
                </li><li>Puissance maximale : 85 cv
                </li><li>Système d’alimentation : 3 carburateurs
                </li><li>Lubrification : Mélange 50 :1
                </li><li>Allumage : Electrique
                </li><li>Mise en route : CDI
                </li><li>Consommation de carburant : 35,0L/h @5500tr/minute</li>',
            'caracteristique_2' => 
                '<li>Poids avec hélice : 111kg-124kg
                </li><li>Longueur d’arbre : Long (521mm) ; Extra-Long (647mm)
                </li><li>Capacité réservoir d’huile : 10L</li>',
            'caracteristique_3' => 
                '<li>TRIM : Electrique
                </li><li>Angle de virage : 30 degrés dans chaque direction
                </li><li>Equipements inclus : Hélice, nourrice, manette latérale, faisceaux, et coupe circuit</li>',
            ],
        ];
        foreach($moteur2temps AS $moteur2temp):
            Moteur2t::create($moteur2temp);
        endforeach;
    }
}
