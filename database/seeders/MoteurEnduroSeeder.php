<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\MoteurEnduro;

class MoteurEnduroSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $moteurEnduros = [
            [
                'product_type_id' => 2,
                'name' => 'E8DMH',
                'image' => 'https://dl.airtable.com/.attachmentThumbnails/fd1d9ac4f4cc3d0bf560d3ef36373b5b/b7f2418a',
                'titre' => 'LA PERFORMANCE',
                'titre_description' => 'Petit mais performant',
                'description' => 
                    '<li>Type de moteur : en ligne 2
                    </li><li>Cylindrée [cm3] : 165
                    </li><li>Alésage x course [mm] : 50,0 × 42,0
                    </li><li>Max. Sortie de l’arbre d’hélice [kW (ps)] @r/min : 5,9 (8) @5000
                    </li><li>Carburateur du système d’admission de carburant
                    </li><li>Hauteur du tableau arrière du moteur [mm (pouces)] : S : 442 (17,4), L : 582 (22,9)
                    </li><li>Poids à sec [kg] * : 28-29
                    </li><li>Démarrage du système : démarrage manuel
                    </li><li>Commandes : Barre franche
                    </li><li>Système d’inclinaison : Inclinaison manuelle
                    </li><li>Système de lubrification : Carburant et huile pré-mélangés</li>',
                'prix' => '260 000 XPF TTC',
                'caracteristique_1' => 
                    '<li>Type de moteur : 2 temps
                    </li><li>Cylindrée : 165 cm3
                    </li><li>Architecture : 
                    </li><li>Alésage X Course : 50.0×42.0
                    </li><li>Plage de fonctionnement à plein régime (r/min) : 4 500/5 500 
                    </li><li>Rendement maxi de l’arbre porte-hélice (kW/ps) : 5.9/8
                    </li><li>Système d’alimentation : carburateur 
                    </li><li>Lubrification : pré-mélange à 2%
                    </li><li>Allumage : Électronique (CDI)
                    </li><li>Mise en route : 
                    </li><li>Rapport de démultiplication
                    </li><li>Consommation de carburant</li>',
                'caracteristique_2' => 
                    '<li>Hauteur du tableau recommandée
                    </li><li>Poids avec hélice
                    </li><li>Dimension arbre court/long/entra long/ultra long
                    </li><li>Capacité réservoir d’essence
                    </li><li>Capacité réservoir d’huile</li>',
                'caracteristique_3' => 
                    '<li>TRIM : Manuel, 4 positions
                    </li><li>Angle de virage : 360 degrés
                    </li><li>Equipements inclus : Hélice, nourrice et coupe circuit</li>',
            ],
            [
                'product_type_id' => 2,
                'name' => 'E15DMH',
                'image' => 'https://dl.airtable.com/.attachmentThumbnails/e0d6d7bad5386cc6212a87a2e77ac0bc/7ea84fea',
                'titre' => 'LA PERFORMANCE',
                'titre_description' => 'Efficacité et performance',
                'description' => 
                    '<li>Type de moteur : en ligne 2
                    </li><li>Cylindrée [cm3] : 246
                    </li><li>Alésage x course [mm] : 56,0 x 50,0
                    </li><li>Max. Sortie de l’arbre d’hélice [kW (ps)] : @r/min 11,0 (15) @5000
                    </li><li>Carburateur du système : d’admission de carburant
                    </li><li>Hauteur du tableau arrière du moteur [mm (pouces)] : S : 441 (17,4), L : 568 (22,4), X : 710 (30,3)
                    </li><li>Poids à sec [kg] * : 38-41
                    </li><li>Démarrage du système : démarrage manuel
                    </li><li>Commandes : Barre franche
                    </li><li>Système d’inclinaison : Inclinaison manuelle
                    </li><li>Système de lubrification : Carburant et huile pré-mélangés</li>',
                'prix' => '335 000 XPF TTC',
                'caracteristique_1' => 
                    '<li>Cylindrée (cm3) 246 cm3
                    </li><li>Nombre de cylindre 
                    </li><li>Taux de compression 7,0
                    </li><li>Lubrification Pré-mélange à 2 
                    </li><li>Carburant Essence ordinaire
                    </li><li>Démarrage Manuel
                    </li><li>Direction Barre franche
                    </li><li>Consommation (l./h.) 7,7 l./h.
                    </li><li>Poids (kg) 38 kg – 40 kg
                    </li><li>Longueur Embase S :442mm/ L: 569mm
                    </li><li>Alésage X Course : 56.0×50.0</li>',
                'caracteristique_2' => 
                    '<li>Hauteur du tableau recommandée
                    </li><li>Poids avec hélice
                    </li><li>Dimension arbre court/long/entra long/ultra long
                    </li><li>Capacité réservoir d’essence
                    </li><li>Capacité réservoir d’huile</li>',
                'caracteristique_3' => 
                    '<li>TRIM : Manuel, 4 positions
                    </li><li>Angle de virage : 40 degrés dans chaque direction
                    </li><li>Equipements inclus : Hélice, nourrice et coupe circuit</li>',
            ],
            [
                'product_type_id' => 2,
                'name' => 'E25BMH',
                'image' => 'https://dl.airtable.com/.attachmentThumbnails/5d9a69daeb6a4d8231ae7135605bcb55/868307fe',
                'titre' => 'LA PERFORMANCE',
                'titre_description' => 'Fiable et Robuste',
                'description' => 
                    '<li>Type de moteur : en ligne 2
                    </li><li>Cylindrée [cm3] : 496
                    </li><li>Alésage x course [mm] : 72,0 x 61,0
                    </li><li>Max. Sortie de l’arbre d’hélice [kW (ps)] : @r/min 18,4 (25) @5000
                    </li><li>Carburateur du système : d’admission de carburant
                    </li><li>Hauteur du tableau arrière du moteur [mm (pouces)] : S : 423 (16,7), L : 550 (21,7)
                    </li><li>Poids à sec [kg] * : 53-55
                    </li><li>Démarrage du système : démarrage manuel
                    </li><li>Commandes : Barre franche
                    </li><li>Système d’inclinaison : Inclinaison manuelle
                    </li><li>Système de lubrification : Carburant et huile pré-mélangés</li>',
                'prix' => '380 000 XPF TTC',
                'caracteristique_1' => 
                    '<li>Cylindrée (cm3) 496 cm3
                    </li><li>Nombre de cylindre 2
                    </li><li>Taux de compression 6,2
                    </li><li>Lubrification Pré-mélange à 2 %
                    </li><li>Carburant Essence ordinaire
                    </li><li>Démarrage Manuel
                    </li><li>Direction Barre franche
                    </li><li>Consommation (l./h.) 10,7 l./h.
                    </li><li>Poids (kg) 52 kg – 55kg
                    </li><li>Longueur Embase S :423mm/ L: 550mm
                    </li><li>Alésage X Course : 72.0×61.0</li>',
                'caracteristique_2' => 
                    '<li>Hauteur du tableau recommandée
                    </li><li>Poids avec hélice
                    </li><li>Dimension arbre court/long/entra long/ultra long
                    </li><li>Capacité réservoir d’essence
                    </li><li>Capacité réservoir d’huile</li>',
                'caracteristique_3' => 
                    '<li>TRIM : Manuel, 5 positions
                    </li><li>Angle de virage : 40 degrés dans chaque direction
                    </li><li>Equipements inclus : Hélice, nourrice et coupe circuit</li>',
            ],
            [
                'product_type_id' => 2,
                'name' => 'E40XMH',
                'image' => 'https://dl.airtable.com/.attachmentThumbnails/434d50822b3f0721752c8b55c305ed5e/297c5daa',
                'titre' => 'LA PERFORMANCE',
                'titre_description' => 'Choissir la fiabilité',
                'description' => 
                    '<li>Moteur 2 cylindres
                    </li><li>Démarrage manuel
                    </li><li>Bar franche
                    </li><li>Cylindrée: 703 cm³
                    </li><li>Poids: 72,81 kgs</li>',
                'prix' => '490 000 XPF TTC',
                'caracteristique_1' => 
                    '<li>Type : 2 TEMPS
                    </li><li>Cylindrée : 703 cm3
                    </li><li>Architecture : 
                    </li><li>Alésage X Course : 80.0×70.0
                    </li><li>Plage de fonctionnement à plein régime : 4 500/5 500 
                    </li><li>Système d’alimentation : carburateur 
                    </li><li>Lubrification : pré-mélange à 2% 
                    </li><li>Allumage : Électronique (CDI)
                    </li><li>Mise en route : 
                    </li><li>Rapport de démultiplication
                    </li><li>Consommation de carburant</li>',
                'caracteristique_2' => 
                    '<li>Hauteur du tableau recommandée
                    </li><li>Poids avec hélice
                    </li><li>Dimension arbre court/long/entra long/ultra long
                    </li><li>Capacité réservoir d’essence
                    </li><li>Capacité réservoir d’huile</li>',
                'caracteristique_3' => 
                    '<li>TRIM : Manuel, 7 positions
                    </li><li>Angle de virage : 45 degrés dans chaque direction
                    </li><li>Equipements inclus : Hélice, nourrice et coupe circuit</li>',
            ],
            [
                'product_type_id' => 2,
                'name' => 'E60HMHD',
                'image' => 'https://dl.airtable.com/.attachmentThumbnails/57c00e932cd2dc96291781b5125025d9/c720522b',
                'titre' => 'LA PERFORMANCE',
                'titre_description' => 'Robuste face à l\'adversité',
                'description' => 
                    '<li>Moteur 2 cylindres
                    </li><li>Démarrage manuel
                    </li><li>Bar franche
                    </li><li>Cylindrée: 849 cm³
                    </li><li>Poids: 96-105 kgs</li>',
                'prix' => '650 000 XPF TTC',
                'caracteristique_1' => 
                    '<li>Type : 2 TEMPS
                    </li><li>Cylindrée (cm3) 849cm3
                    </li><li>Nombre de cylindre 3
                    </li><li>Taux de compression 6,1
                    </li><li>Lubrification Pré-mélange à 2 %
                    </li><li>Carburant Essence ordinaire
                    </li><li>Démarrage Manuel
                    </li><li>Direction Barre franche + Hydro Trim & Tilt
                    </li><li>Consommation (l./h.) 25,5 l./h.
                    </li><li>Poids (kg) 102 kg
                    </li><li>Longueur Embase L: 571mm
                    </li><li>Alésage X Course : 72.0×69.5</li>',
                'caracteristique_2' => 
                    '<li>Hauteur du tableau recommandée
                    </li><li>Poids avec hélice
                    </li><li>Dimension arbre court/long/entra long/ultra long
                    </li><li>Capacité réservoir d’essence
                    </li><li>Capacité réservoir d’huile</li>',
                'caracteristique_3' => 
                    '<li>TRIM : Manuel, 5 positions
                    </li><li>TRIM eau peu profonde : Assistance hydraulique
                    </li><li>Angle de virage : 45 degrés dans chaque direction
                    </li><li>Equipements inclus : Hélice, nourrice et coupe circuit</li>',
            ],
            [
                'product_type_id' => 2,
                'name' => 'E75BMH',
                'image' => 'https://dl.airtable.com/.attachmentThumbnails/8c4590bb8e097bfce75434dd8739562c/b469556f',
                'titre' => 'LA PERFORMANCE',
                'titre_description' => 'Le plus puissant d\'entre tous',
                'description' => 
                    '<li>Moteur 3 cylindres
                    </li><li>Démarrage manuel
                    </li><li>Bar franche
                    </li><li>Cylindrée: 1140 cm³
                    </li><li>Poids: 112-121 kgs</li>',
                'prix' => '745 000 XPF TTC',
                'caracteristique_1' => 
                    '<li>Type : 2 TEMPS
                    </li><li>Cylindrée (cm3) 1140cm3
                    </li><li>Nombre de cylindre 3
                    </li><li>Taux de compression 4,5
                    </li><li>Lubrification Pré-mélange à 2 %
                    </li><li>Carburant Essence ordinaire
                    </li><li>Démarrage Manuel
                    </li><li>Direction Barre franche + Hydro Trim & Tilt
                    </li><li>Consommation (l./h.) 34 l./h.
                    </li><li>Poids (kg) 112 kg
                    </li><li>Longueur Embase L : 521mm
                    </li><li>Alésage X Course : 82.0×72.0</li>',
                'caracteristique_2' => 
                    '<li>Hauteur du tableau recommandée
                    </li><li>Poids avec hélice
                    </li><li>Dimension arbre court/long/entra long/ultra long
                    </li><li>Capacité réservoir d’essence
                    </li><li>Capacité réservoir d’huile</li>',
                'caracteristique_3' => 
                    '<li>TRIM : Manuel, 5 positions
                    </li><li>TRIM eau peu profonde : Assistance hydraulique
                    </li><li>Angle de virage : 30 degrés dans chaque direction
                    </li><li>Equipements inclus : Hélice, nourrice et coupe circuit</li>',
            ],
        ];
        foreach($moteurEnduros AS $moteurEnduro):
            MoteurEnduro::create($moteurEnduro);
        endforeach;
    }
}
