<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Moteur4tFt;

class Moteur4tFtSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $moteur4tempsFts = [
        [
            'product_type_id' => 2,
            'name' => 'FT50CEHDL',
            'image' => 'https://dl.airtable.com/.attachmentThumbnails/e20000ff7327e2743f9ad35065aee297/7d88d385',
            'titre' => 'FT',
            'description' => 
                '<li>Moteur :  4T, 4 cylindres, SOHC</li>
                <li>Cylindrée :  935 cc</li>
                <li>Puissance maximale :   50 CV à 5 500 tr/min</li>
                <li>Système de Combustible:  4 Carburateurs</li>
                <li>Système d\'Encendido :  CDI Micro Ordinateur</li>
                <li>Système de Arranque:  Electrique</li>
                <li>Système de Dirección :  Manuel</li>
                <li>Consommation de combustible :   16 Lt/Hr</li>
                <li>Bascule et Inclinaison:  Power Trim/Manuel</li>
                <li>Matériel de Propela:  Acier Inoxidable</li>',
            'prix' => '780 000 XPF TTC',
            'caracteristique_1' => 
                '<li>Type : 4 temps, 4 cylindres en ligne, SOHC
</li>           <li>Cylindrée : 935cc
</li>           <li>Bougie : DPR6EA-9
</li>           <li>Alésage X Course : 63.0X75.0mm
</li>           <li>Taux de compression : 9.3 :1
</li>           <li>Plage de régime à plein gaz : 5000 - 6000tr/minute
</li>           <li>Puissance maximale : 36.8kw (50cv)
</li>           <li>Système d’alimentation : 4 carburateurs
</li>           <li>Lubrification : Carter humide
</li>           <li>Capacité en huile : 2.2 litres
</li>           <li>Allumage : Electronique (CDI)
</li>           <li>Mise en route : Électrique avec prime Start™
</li>           <li>Consommation de carburant : 16,0L/h @5500tr/minute
</li>',
            'caracteristique_2' => 
                '<li>Poids avec hélice : 117-121kg
 </li>            <li>Longueur d’arbre : Long(536mm) X-Long(647mm)
</li>             <li>Capacité réservoir d’essence : 24L
</li>',
            'caracteristique_3' => 
                '<li>TRIM : Hydro-5 positions
</li>            <li>Angle de virage : 40 degrés de chaque côté
</li>            <li>Sortie de l\'alternateur : 10Ah
</li>            <li>Contrôle : Commande à distance
</li>            <li>Accessoires inclus : Barre franche, coupe circuit, nourrice essence, hélice
                </li>',
            ],
            [
            'product_type_id' => 2,
            'name' => 'FT50CETL ',
            'image' => 'https://dl.airtable.com/.attachmentThumbnails/085d8d2ab6a598d42d1e087dae375628/6fc8256e',
            'titre' => 'FT',
            'description' => 
                '<li>Type de moteur : 4 temps
</li>             <li>Démarrage : Électrique avec Prime Start™
</li>             <li>Puissance hélice à mi-régime (kW) : 36.8 (50 ch)
</li>             <li>Longueur d\'arbre (mm) : L: 536 (21.1 inch)
</li>             <li>Poids à vide avec hélice(kg) : 121
</li>',
            'prix' => '890 000 XPF TTC',
            'caracteristique_1' => 
                '<li>Type : 4 temps, 4 cylindres en ligne, SOHC
</li>            <li>Cylindrée : 935cc
</li>            <li>Bougie : DPR6EA-9
</li>            <li>Alésage X Course : 63.0X75.0mm
</li>            <li>Taux de compression : 9.3 :1
</li>            <li>Plage de régime à plein gaz : 5000 - 6000tr/minute
</li>            <li>Puissance maximale : 36.8kw (50cv)
</li>            <li>Système d’alimentation : 4 carburateurs
</li>            <li>Lubrification : Carter humide
</li>            <li>Capacité en huile : 2.2 litres
</li>            <li>Allumage : Electronique (CDI)
</li>            <li>Mise en route : Électrique avec prime Start™
</li>            <li>Consommation de carburant : 16,0L/h @5500tr/minute
                </li>',
            'caracteristique_2' => 
                '<li>Poids avec hélice : 117-121kg
</li>            <li>Longueur d’arbre : Long(536mm) X-Long(647mm)
</li>            <li>Capacité réservoir d’essence : 24L
</li>',
            'caracteristique_3' => 
                '<li>TRIM : Electrique-position variable
</li>            <li>Angle de virage : 40 degrés de chaque côté
</li>            <li>Sortie de l\'alternateur : 10Ah
</li>            <li>Contrôle : Commande à distance
</li>            <li>Accessoires inclus : nourrice essence, manette latérale, coupe circuit, hélice, faisceaux
                </li>',
            ],
            [
            'product_type_id' => 2,
            'name' => 'FT60GETL',
            'image' => 'https://dl.airtable.com/.attachmentThumbnails/4e0d41108d53a9346ae9e211c2717b49/2ec5adc5',
            'titre' => 'FT',
            'description' => 
                '<li>Rapports de démultiplication spéciaux pour une poussée maximale
</li>            <li>Hélice à double poussée pour une forte poussée, à la fois en avant et en arrière
</li>            <li>Système Power Trim & Tilt avec large gamme
</li>            <li>Système de démarrage PrimeStart ™ - aussi simple que de démarrer votre voiture
</li>            <li>Guidon multifonction (disponible en option)
</li>            <li>Conception de moteur compacte et efficace
</li>            <li>Puissance remarquable et couple impressionnant
</li>            <li>Inflammation CDI
</li>            <li>Alternateur puissant
</li>            <li>Niveau d\'eau peu profonde pour manœuvrer en toute sécurité dans des eaux peu profondes
</li>            <li>Système de rinçage à l\'eau douce
</li>',
            'prix' => '1 050 000 XPF TTC',
            'caracteristique_1' => 
                '<li>Type : 4 temps, 4 Cylindres en ligne, SOHC
</li>            <li>Cylindrée : 996cc
</li>            <li>Bougie : DPR6EB-9
</li>            <li>Alésage X Course : 65.0X75.0mm
</li>            <li>Taux de compression : 9.5 :1
</li>            <li>Plage de régime à plein gaz : 5000 - 6000tr/minute
</li>            <li>Puissance maximale : 44.1kw (60cv)
</li>            <li>Système d’alimentation : Injection (EFI)
</li>            <li>Lubrification : Carter humide
</li>            <li>Capacité en huile : 2.1 litres
</li>            <li>Allumage : Electronique (CDI)
</li>            <li>Mise en route : Electrique
</li>            <li>Consommation de carburant : 19,2L/h @5500tr/minute
                </li>',
            'caracteristique_2' => 
                '<li>Poids avec hélice : 116kg</li>
                <li>Longueur d’arbre : Long(530mm)</li>
                </li>',
            'caracteristique_3' => 
                '<li>TRIM : Électrique-position variable
</li>            <li>Angle de virage : 40 degrés de chaque côté
</li>            <li>Sortie de l\'alternateur : 16Ah(Charge Batterie 9Ah)
</li>            <li>Contrôle : Commande à distance
</li>            <li>Accessoires inclus : Nourrice essence, coupe circuit, hélice, manette latérale, afficheurs numériques, faisceaux
                </li>',
            ],
        ];
        foreach($moteur4tempsFts AS $moteur4tempFt):
            Moteur4tFt::create($moteur4tempFt);
        endforeach;
    }
}
