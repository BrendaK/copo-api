<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\Models\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users =
        [
            [
                'lastname' => 'Brenda',
                'firstname' => 'Kong',
                'email' => 'brendakong26@gmail.com'
            ]
        ];
        foreach($users AS $user):
            $user["password"] = Hash::make("secret");
            $user["email_verified_at"] = '2021-05-27 14:56:04';
            User::create($user);
         endforeach;
    }
}
