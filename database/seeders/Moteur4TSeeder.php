<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Moteur4T;

class Moteur4TSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $moteur4ts = [
            [
            'product_type_id' => 2,
            'name' => 'F40FET',
            'image' => 'https://dl.airtable.com/.attachmentThumbnails/96ca2e732a655554d1944993b522842f/c21526d1',
            'titre' => 'LA PUISSANCE',
            'description' => 'Le système d’injection électronique EFI de Yamaha permet une puissance colossale à des vitesses élevées, une reprise et un couple excellents à mi-régime, ainsi que des économies de carburant substantielles. Le starter automatique PrimeStart™ permet de démarrer sans effort comme une voiture. Et comme il s’agit de moteurs Yamaha, le démarrage s’effectue à chaque fois au doigt et à l’œil. Notre système unique de relevage et de trim de grande amplitude vous confère un contrôle parfait.',
            'prix' => '850 000 XPF TTC',
            'caracteristique_1' =>
            '
               <li> Type de moteur : quatre temps
               </li><li> Cylindrée : 747 cm³
               </li><li> Architecture : 3/en ligne, SOHC
               </li><li> Alésage x course : 65.0 x 75.0 mm
               </li><li> Puissance à l’arbre d’hélice à mi-régime : 29.4 kW à 5,500 tr/min
               </li><li> Plage de régime à pleins gaz : 5,000 – 6,000 tr/min
               </li><li> Lubrification : Carter humide
               </li><li> Système d’alimentation : Injection électronique (EFI)
               </li><li> Allumage / avance : Électronique (CDI)
               </li><li> Mise en route : Électrique avec Prime Start™
               </li><li> Rapport de démultiplication : 2.00 (26:13)</li>
' ,           'caracteristique_2' => 
               '<li>Hauteur de tableau recommandée : S : 414 mm L:536 mm
               </li><li> Poids avec hélice : F40FEDL: 95.0kg,F40FEHDS: 97.0kg,F40FEHDL: 102.0kg,F40FETS: 94.0kg,F40FETL: 98.0kg
               </li><li> Capacité du réservoir d’essence : séparé, 25litres
               </li></li> Capacité en huile : 1.7litres<li>
',            'caracteristique_3' => 
               '<li>Contrôle : Barre franche (EHD), Commande à distance (ET + ED)
               </li><li> Relevage et trim (angles de trim) : Relevage et trim assistés (ET), Relevage assisté hydraulique (ED + EHD)
               </li><li> Alternateur : 12V -17Aavec redresseur/régulateur
               </li><li> Système d’anti-démarrage Y-COP : YCOP en option
               </li><li> Hélice : En option
               </li><li> Contôle électronique de la vitesse de traîne : Avec afficheurs numériques ou barre franche multifonction
               </li><li> Remarque : La puissance est mesurée suivant la norme ICOMIA 28 au niveau de l’arbre d’hélice</li>
',          ],
            [
            'product_type_id' => 2,
            'name' => 'F70AET',
            'image' => 'https://dl.airtable.com/.attachmentThumbnails/d550f62ad5d69d4bf5a6fb12ac6e717d/3b0f99b1',
            'titre' => 'UN PARTENAIRE PUISSANT POUR LE TRAVAIL OU LES LOISIRS',
            'description' => 'D’une polyvalence imbattable, le 70 ch est un moteur poids plume qui propulsera tous les types de bateaux. Il est aussi à l’aise en milieu professionnel que pour les loisirs et les activités sportives. Le moteur 70 ch est le plus léger et le plus puissant quatre temps de sa catégorie.',
            'prix' => '1 090 000 XPF TTC',
            'caracteristique_1' => 
               '<li>Type de moteur : quatre temps
               </li><li> Cylindrée : 996 cm³
               </li><li> Architecture : 4 /en ligne, 16 soupapes, SOHC
               </li><li> Alésage x course : 65.0 x 75.0 mm
               </li><li> Puissance à l’arbre d’hélice à mi-régime : 51.5 kW à 5,800 tr/min
               </li><li> Plage de régime à pleins gaz : 5,300 – 6,300 tr/min
               </li><li> Lubrification : Carter humide
               </li><li> Système d’alimentation : Injection électronique (EFI)
               </li><li> Allumage / avance : Allumage électronique (TCI)
               </li><li> Mise en route : Électrique avec Prime Start™
               </li><li> Rapport de démultiplication : 2.33 (28:12)</li>
',
          'caracteristique_2' => 
               '<li>Hauteur de tableau recommandée : L : 534 mm X:648 mm
               </li><li> Poids avec hélice : F70AETL: 119.0kg,F70AETX: 121.0kg
               </li><li> Capacité du réservoir d’essence : séparé, 25litres
               </li><li> Capacité en huile : 2.1litres</li>
',  
        'caracteristique_3' => 
               '<li>Contrôle : Commande à distance
               </li><li> Relevage et trim (angles de trim) : Relevage et trim assistés
               </li><li> Alternateur : 12V -15Aavec redresseur/régulateur
               </li><li> Système d’anti-démarrage Y-COP : YCOP en option
               </li><li> Hélice : En option
               </li><li> Contôle électronique de la vitesse de traîne : Avec afficheurs numériques ou barre franche multifonction
               </li><li> Système d’amortissement de la transmission (SDS) : En option
               </li><li> Remarque : La puissance est mesurée suivant la norme ICOMIA 28 au niveau de l’arbre d’hélice</li>
',            ],
            [
            'product_type_id' => 2,
            'name' => 'F115BET',
            'image' => 'https://dl.airtable.com/.attachmentThumbnails/1a9ea4acee824ce911dd737c689cb94c/74e8c092',
            'titre' => 'LE MEILLEUR RAPPORT POIDS/PUISSANCE DE SA CATÉGORIE',
            'description' => 'Le 115 ch, novateur et tout en légèreté, est un véritable hymne à la détente et au plaisir sur l’eau : ce moteur est l’un des plus propres et silencieux jamais conçus.',
            'prix' => '1 450 000 XPF TTC',
            'caracteristique_1' => 
               '<li>Type de moteur : quatre temps
               </li><li> Cylindrée : 996 cm³
               </li><li> Architecture : 4 /en ligne, 16 soupapes, SOHC
               </li><li> Alésage x course : 65.0 x 75.0 mm
               </li><li> Puissance à l’arbre d’hélice à mi-régime : 51.5 kW à 5,800 tr/min
               </li><li> Plage de régime à pleins gaz : 5,300 – 6,300 tr/min
               </li><li> Lubrification : Carter humide
               </li><li> Système d’alimentation : Injection électronique (EFI)
               </li><li> Allumage / avance : Allumage électronique (TCI)
               </li><li> Mise en route : Électrique avec Prime Start™
               </li><li> Rapport de démultiplication : 2.33 (28:12)</li>
',            
            'caracteristique_2' => 
               '<li>Hauteur de tableau recommandée : L : 534 mm X:648 mm
               </li><li> Poids avec hélice : F70AETL: 119.0kg,F70AETX: 121.0kg
               </li><li> Capacité du réservoir d’essence : séparé, 25litres
               </li><li> Capacité en huile : 2.1litres</li>
',            
            'caracteristique_3' => 
               '<li>Contrôle : Commande à distance
               </li><li> Relevage et trim (angles de trim) : Relevage et trim assistés
               </li><li> Alternateur : 12V -15Aavec redresseur/régulateur
               </li><li> Système d’anti-démarrage Y-COP : YCOP en option
               </li><li> Hélice : En option
               </li><li> Contôle électronique de la vitesse de traîne : Avec afficheurs numériques ou barre franche multifonction
               </li><li> Système d’amortissement de la transmission (SDS) : En option
               </li><li> Remarque : La puissance est mesurée suivant la norme ICOMIA 28 au niveau de l’arbre d’hélice</li>
',            ],
            [
            'product_type_id' => 2,
            'name' => 'F150DET',
            'image' => 'https://dl.airtable.com/.attachmentThumbnails/bc65a80c8321cdb04711afe57d7e27e3/79ee51c4',
            'titre' => 'UNE SOUPLESSE ET DES PERFORMANCES INÉGALÉES',
            'description' => 'Le 115 ch, novateur et tout en légèreté, est un véritable hymne à la détente et au plaisir sur l’eau : ce moteur est l’un des plus propres et silencieux jamais conçus.',
            'prix' => '1 450 000 XPF TTC',
            'caracteristique_1' => 
               '<li>Type
               </li><li> Cylindrée
               </li><li> Architecture
               </li><li> Alésage X Course
               </li><li> Plage de régime à plein gaz
               </li><li> Puissance maximale
               </li><li> Système d’alimentation
               </li><li> Lubrification
               </li><li> Allumage
               </li><li> Mise en route
               </li><li> Rapport de démultiplication
               </li><li> Consommation de carburant</li>
',            
            'caracteristique_2' => 
               '<li>Hauteur du tableau recommandée
               </li><li> Poids avec hélice
               </li><li> Dimension arbre court/long/entra long/ultra long
               </li><li> Capacité réservoir d’essence
               </li><li> Capacité réservoir d’huile</li>
',            
            'caracteristique_3' => '',
            ],
            [
            'product_type_id' => 2,
            'name' => 'F175CET',
            'image' => 'https://dl.airtable.com/.attachmentThumbnails/f0e4a153b1e52fc88de756ac0f778768/7a1d48c7',
            'titre' => 'POUR PROFITER DES PERFORMANCES DE NOTRE MOTEUR QUATRE CYLINDRES',
            'description' => 'À l’instar de tous nos moteurs EFI, le 175 ch s’associe directement au système de réseau exclusif à Yamaha afin que vous bénéficiez d’un large choix de jauges et d’instruments numériques élaboré            ',
            'prix' => '2 100 000 XPF TTC',
            'caracteristique_1' => 
               '<li>Type de moteur : quatre temps
               </li><li> Cylindrée : 2,785 cm³
               </li><li> Architecture : 4/en ligne, 16 soupapes, DOHC
               </li><li> Alésage x course : 96.0 x 96.2 mm
               </li><li> Puissance à l’arbre d’hélice à mi-régime : 128.7 kW à 5,500 tr/min
               </li><li> Plage de régime à pleins gaz : 5,000 – 6,000 tr/min
               </li><li> Lubrification : Carter humide
               </li><li> Système d’alimentation : Injection électronique (EFI)
               </li><li> Allumage / avance : Allumage électronique (TCI)
               </li><li> Mise en route : Électrique avec Prime Start™
               </li><li> Rapport de démultiplication : 1.86 (26:14)</li>
',            
            'caracteristique_2' => 
               '<li>Hauteur de tableau recommandée : L : 516 mm X:643 mm
               </li><li> Poids avec hélice : F175CETL: 226.0kg,F175CETX:227.0kg,FL175CETX:227.0kg
               </li><li> Capacité du réservoir d’essence : –
               </li><li> Capacité en huile : 4.5litres</li>
',            
            'caracteristique_3' => '',
            ],
            [
            'product_type_id' => 2,
            'name' => 'F250DET',
            'image' => 'https://dl.airtable.com/.attachmentThumbnails/1a0292d2b517c9d6a0a6473a00ac8c7d/8e637e4c',
            'titre' => 'UN MOTEUR V6 POUR VOS AVENTURES EN HAUTE MER',
            'description' => 'Pour des performances homogènes, silencieuses et respectueuses de l’environnement, un rendement énergétique optimal et un démarrage simplifié, notre V6 à 24 soupapes et double arbre à cames en tête est équipé d’une injection électronique (EFI) et d’un calage variable des arbres à cames (VCT). Associant la technologie de pointe au style, la conception racée et compacte de ce modèle parle d’elle-même.',
            'prix' => '3 000 000 XPF TTC',
            'caracteristique_1' => 
               '<li>Type de moteur : quatre temps
               </li><li> Cylindrée : 4169 cm³
               </li><li> Architecture : V6 (60°), 24 soupapes, DOHC with VCT
               </li><li> Alésage x course : 96.0 x 96.0 mm
               </li><li> Puissance à l’arbre d’hélice à mi-régime : 183.8 kW à 5,500 tr/min
               </li><li> Plage de régime à pleins gaz : 5,000 – 6,000 tr/min
               </li><li> Lubrification : Carter humide
               </li><li> Système d’alimentation : Injection électronique (EFI)
               </li><li> Allumage / avance : Allumage électronique (TCI)
               </li><li> Mise en route : Électrique avec Prime Start™
               </li><li> Rapport de démultiplication : 1.75 (21:12)</li>
',            
            'caracteristique_2' => 
               '<li>Hauteur de tableau recommandée : X : 643 mm U:770 mm
               </li><li> Poids avec hélice : F250DETX: 260.0kg,FL250DETX: 260.0kg,F250DETU: 268.0kg,FL250DETU: 268.0
               </li><li> Capacité du réservoir d’essence : –
               </li><li> Capacité en huile : 6.3litres</li>
',            
            'caracteristique_3' => '',
            ],
            [
            'product_type_id' => 2,
            'name' => 'F300BET',
            'image' => 'https://dl.airtable.com/.attachmentThumbnails/db407a713acedadad9297ba76bbbde34/91b6274b',
            'titre' => 'UN V6 EXCEPTIONNEL POUR LA NAVIGATION EN HAUTE MER',
            'description' => 'Pour des performances homogènes, silencieuses et respectueuses de l’environnement, un rendement énergétique optimal et un démarrage simplifié, notre V6 à 24 soupapes et double arbre à cames en tête est équipé d’une injection électronique (EFI) et d’un calage variable des arbres à cames (VCT). Associant la technologie de pointe au style, la conception racée et compacte de ce modèle parle d’elle-même.',
            'prix' => 'La F300BETX  à partir de 3 300 000 XPF TTC et la F300BETU à partir de 3 400 000 XPF TTC ',
            'caracteristique_1' => 
               '<li>Type de moteur : quatre temps
               </li><li> Cylindrée : 4169 cm³
               </li><li> Architecture : V6 (60°), 24 soupapes, DOHC with VCT
               </li><li> Alésage x course : 96.0 x 96.0 mm
               </li><li> Puissance à l’arbre d’hélice à mi-régime : 220.6 kW à 5,500 tr/min
               </li><li> Plage de régime à pleins gaz : 5,000 – 6,000 tr/min
               </li><li> Lubrification : Carter humide
               </li><li> Système d’alimentation : Injection électronique (EFI)
               </li><li> Allumage / avance : Allumage électronique (TCI)
               </li><li> Mise en route : Électrique avec Prime Start™
               </li><li> Rapport de démultiplication : 1.75 (21:12)</li>
',            
            'caracteristique_2' => 
               '<li>Hauteur de tableau recommandée : X : 643 mm U:770 mm
               </li><li> Poids avec hélice : F300BETX: 260.0kg,FL300BETX: 260.0kg,F300BETU: 268.0kg,FL300BETU: 268.0kg
               </li><li> Capacité du réservoir d’essence : –
               </li><li> Capacité en huile : 6.3litres</li>
',            
            'caracteristique_3' => '',
            ],
        ];
        foreach($moteur4ts AS $moteur4t):
            Moteur4t::create($moteur4t);
        endforeach;
    }
}
