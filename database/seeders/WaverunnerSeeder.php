<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Waverunner;

class WaverunnerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $waverunners = [
            [
            'product_type_id' => 2,
            'name' => '',
            'image' => '',
            'titre' => '',
            'petit_titre' => '',
            'description' => '',
            'prix' => '',
            'image_force1' => '',
            'force1' => '',
            'image_force2' => '',
            'force2' => '',
            'image_force3' => '',
            'force3' => '',
            'image_force4' => '',
            'force4' => '',
            'image_force5' => '',
            'force5' => '',
            'caractéristique' => '',
            ],
        ];
        foreach($waverunners AS $waverunner):
            Waverunner::create($waverunner);
        endforeach;
    }
}
