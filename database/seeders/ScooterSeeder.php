<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Scooter;

class ScooterSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $scooters = [
            [
            'product_type_id' => 1,
            'name' => 'Tmax 2021',
            'video' => 'UZ7c5WrTNEM',
            'image' => 'https://dl.airtable.com/.attachmentThumbnails/8eba6b33d6e2aba100414cdb1d42c39b/35dca5d9',
            'titre' => 'Tmax 2021',
            'petit_titre' => 'Toujours au MAX',
            'description' => 'Depuis plus de 20 ans c’est le leader des scooters sportifs. Dès sa sortie il fait tourner les têtes, fait rêver les gens et sillonne les routes du Fenua. Le concept est simple et détonnant : combiner les avantages des motos et des scooters en un seul engin. Souvent imité, jamais égalé, le TMAX est la référence de sa catégorie et il revient en force avec un nouveau moteur et un design dont seul Yamaha a le secret. Pas de compromis, rien que le max.',
            'prix' => '1 990 000 XPF TTC',
            'image_force1' => 'https://dl.airtable.com/.attachmentThumbnails/6c253c5f6437d1abdd9197c113cbf5a9/32c97d01',
            'force1' => 'Moteur de 560cc : Le TMAX 560 est plus rapide, plus sportif et plus dynamique que l’ancienne génération grâce à son moteur mis à jour à la norme EURO5. Avec 6% de couple en plus, le scooter bénéficie d’accélérations encore plus instantanées. Avec son système d’équilibrage spécifique, ce moteur bicylindre est l’un des plus souples et des plus compacts de sa catégorie.',
            'image_force2' => 'https://dl.airtable.com/.attachmentThumbnails/7f8edb67dc583d4f88976f8f908b4b70/a344e1b1',
            'force2' => 'Les technologies de séries : Le système « Smart Key », le tableau de bord TFT monochrome, le D-Mode, l’ABS, le TCS, la béquille centrale verrouillable et le frein à main sont des équipements qui font leurs preuves depuis plusieurs générations de TMAX. Tout nos maxi scooters viennent équipés de série avec ces avancées technologiques.',
            'image_force3' => 'https://dl.airtable.com/.attachmentThumbnails/c496c71e404d00172a824328de4782d8/4fe08444',
            'force3' => 'Grand coffre sous la selle : En plus d’être le modèle sportif le plus léger de sa catégorie, le TMAX vous offre également le plus grand coffre sous la selle. Ce coffre peut accueillir un casque intégral ou deux casques semi-jets Pour encore plus de confort il s’éclaire automatiquement. En option vous pouvez également équiper votre TMAX d’un top case pour bénéficier d’au moins 45L d’espace de rangement supplémentaire.',
            'image_force4' => 'https://dl.airtable.com/.attachmentThumbnails/a996d1bb143691f74fc17307e0985753/5dea6e41',
            'force4' => 'Une ergonomie hors-pair : Ce qui a fait du TMAX une référence dès son lancement c’est bien son ergonomie et ses performances. Piloter un TMAX est une sensation unique. Son cadre en aluminium coulé sous pression et d’un long bras oscillant n’est pas sans rappeler un châssis de moto. Le moteur est monté directement dans le cadre afin d’améliorer la rigidité du châssis et garantir maniabilité, précision et agilité.',
            'image_force5' => 'https://dl.airtable.com/.attachmentThumbnails/f347e7ccad20f9fefa2f26f66ec3488a/cdbf418b',
            'force5' => 'Habillage dynamique et sportif : Cette septième génération de TMAX est dotée d’un nouveau look dynamique. Le feu arrière en forme de T, des caches et ailes redessinées ainsi que les prises d’air viennent ajouter à la sportivité de ce maxi scooter. La finition est toujours de haute qualité. L’assemblage est millimétré, les matériaux de qualité et les plastiques peints et vernis.',
            'caracteristique_1' => 
                '<li>Type : Quatre soupapes, Double arbre à cames en tête, Quatre temps, Refroidissement liquide, EURO5, Bicylindre en ligne
                </li><li>Cylindrée : 562cc
                </li><li>Alésage X Course : 70,0 × 73,0 mm
                </li><li>Taux de compression : 10.9:1
                </li><li>Couple maximal : 55,7 Nm à 5 250 tr/min
                </li><li>Puissance maximale : 35,0 kW à 7 500 tr/min (47,5cv)
                </li><li>Système d’alimentation : Injection électronique
                </li><li>Lubrification : Carter sec
                </li><li>Allumage : Allumage électronique (TCI)
                </li><li>Mise en route : Démarreur électrique
                </li><li>Transmission : Automatique à courroie trapézoïdale
                </li><li>Consommation de carburant : 4,8L/100km
                </li><li>Emission CO2 : 112g/km</li>',
            'caracteristique_2' => 
                '<li>Longueur hors tout : 2200mm
                </li><li>Largeur hors tout : 765mm
                </li><li>Hauteur hors tout : 1420mm
                </li><li>Hauteur de selle : 800mm
                </li><li>Empattement : 1575mm
                </li><li>Garde au sol minimale : 125mm
                </li><li>Poids tous pleins faits : 218kg
                </li><li>Capacité réservoir d’essence : 15L</li>',
            'caracteristique_3' => 
                '<li>Suspension avant : Fourche télescopique
                </li><li>Débattement avant : 120mm
                </li><li>Suspension arrière : Bras oscillant
                </li><li>Débattement arrière : 117mm
                </li><li>Frein avant : Double frein à disque de 267 mm de diamètre à commande hydraulique
                </li><li>Frein arrière : Frein à disque de 282 mm de diamètre à commande hydraulique
                </li><li>Pneu avant : 120/70R15M/C 56H tubeless
                </li><li>Pneu arrière : 160/60R15M/C 67H tubeless</li>',
            ],
            [
            'product_type_id' => 1,
            'name' => 'XMAX 300',
            'video' => 'rpl6qr4K0Ik',
            'image' => 'https://dl.airtable.com/.attachmentThumbnails/cf39831ffa92236a067a2252ec49cdb8/c7e8913e',
            'titre' => 'XMAX 300',
            'petit_titre' => 'Conçu pour le MAX',
            'description' => 'La famille MAX s’agrandit et accueille le XMAX 300 dans ses rangs. Avec un design inspiré de son grand frère, le TMAX, le cadet intègre aussi ses technologies et sa fourche de moto. Parfaitement adapté à un milieu urbain, il est aussi très polyvalent. Confort de conduite, grand rangement sous la selle, ABS, TCS ce scooter sportif saura s’adapter aux routes de Fenua.',
            'prix' => '550 000 XPF TTC',
            'image_force1' => 'https://dl.airtable.com/.attachmentThumbnails/2504e655629b54a32fda9566105be025/6adbf322',
            'force1' => 'Le Design : Dès le premier regard le XMAX 300 ne passe pas inaperçu et rappelle étrangement le plus célèbre maxi scooter de la Polynésie. En effet, il a été conçu dans la pure tradition du TMAX. L’habillage aérodynamique, les phares à double optique à LED et les caches latéraux en forme de boomerang sont tous inspirés de l’emblématique maxi scooter et attestent de la qualité de finition du XMAX 300.',
            'image_force2' => 'https://dl.airtable.com/.attachmentThumbnails/aa9ec3b983ccb44a9040af92a6e4dffc/c1ebe51e',
            'force2' => 'Moteur Blue Core 300cc : La technologie BLUE CORE permet d’augmenter le rendement du moteur, et par conséquent, il délivre plus de puissance avec moins de carburant. Le moteur du XMAX 300 est ainsi doté de soupapes optimisées, d’une chambre de combustion compacte et d’un calage optimum. Ces paramètres permettent un gain de puissance et d’efficacité pour des performances accrues et une consommation d’essence moindre.',
            'image_force3' => 'https://dl.airtable.com/.attachmentThumbnails/9b2ebad0212c7e73ef082804eb0821ad/e779baa1',
            'force3' => 'Le confort au MAX : La technologie BLUE CORE permet d’augmenter le rendement du moteur, et par conséquent, il délivre plus de puissance avec moins de carburant. Le moteur du XMAX 300 est ainsi doté de soupapes optimisées, d’une chambre de combustion compacte et d’un calage optimum. Ces paramètres permettent un gain de puissance et d’efficacité pour des performances accrues et une consommation d’essence moindre.',
            'image_force4' => 'https://dl.airtable.com/.attachmentThumbnails/8037ca68541dcc04098250634cdfb445/75e299ef',
            'force4' => 'Les technologies embarquées de séries : Le XMAX 300 est équipé d’une multitude de technologies vous garantissant confort et sécurité. Le contacteur « SMART KEY » vous permet de démarrer votre scooter sans avoir besoin de chercher vos clefs dans les poches ou dans votre sac. Le système de contrôle de la traction (TCS) et l’ABS couplé aux freins à disques avant et arrière vous assurent un contrôle sûr et une sécurité accrue au quotidien ou par mauvais temps. L’éclairage intégral à LED vous permet de voir et d’être vu dans toutes les conditions du jour et de la nuit.',
            'image_force5' => '',
            'force5' => '',
            'caracteristique_1' => 
                '<li>Type : Quatre soupapes, Quatre temps, Monocylindre, Refroidissement liquide, EURO5, Simple arbre à cames en tête
                </li><li>Cylindrée : 292cc
                </li><li>Alésage X Course : 70X75.8mm
                </li><li>Taux de compression : 10.9 : 1
                </li><li>Couple maximal : 29.0 N.m à 5,750 tr/min
                </li><li>Puissance maximale : 20.6 kW (20.63cv) à 7,250 tr/min
                </li><li>Système d’alimentation : Injection électronique
                </li><li>Lubrification : Carter humide
                </li><li>Allumage : Electronique (TCI)
                </li><li>Mise en route : Démarreur électrique
                </li><li>Transmission : Automatique à courroie trapézoïdale
                </li><li>Consommation de carburant : 3.0L/100km</li>',
            'caracteristique_2' => 
                '<li>Longueur hors tout : 2,185 mm
                </li><li>Largeur hors tout : 775 mm
                </li><li>Hauteur hors tout : 1,415 mm
                </li><li>Hauteur de selle : 795 mm
                </li><li>Empattement : 1,540 mm
                </li><li>Garde au sol minimale : 135 mm
                </li><li>Poids tous pleins faits : 180kg
                </li><li>Capacité réservoir d’essence : 13l</li>',
            'caracteristique_3' => 
                '<li>Suspension avant : Fourche télescopique
                </li><li>Débattement avant : 110 mm
                </li><li>Suspension arrière : Moteur oscillant
                </li><li>Débattement arrière : 79 mm
                </li><li>Frein avant : Simple disque , Ø267 mm</li>',
            ],
            [
            'product_type_id' => 1,
            'name' => 'NMAX 125 2021',
            'video' => 'QICRwkBo0t8',
            'image' => 'https://dl.airtable.com/.attachmentThumbnails/8ceed2ac94c28b4aee4dc79506e0c349/88a71eda',
            'titre' => 'NMAX 125 2021',
            'petit_titre' => 'FAIRE CORPS AVEC LA VILLE',
            'description' => 'Le scooter 125cc leader du marché Polynésien fait peau neuve. Nouvel habillage sportif, nouveau châssis, nouveaux feux avant et arrière tout LED, nouvelles technologies de série et nouveau moteur. Yamaha met le Max dans la version 2021 de votre scooter préféré. Si vous l’avez déjà essayé cette version saura vous reconquérir. En conduite urbaine ou sur les trajets plus longs il s’adapte et mange les kilomètres avec une autonomie étonnante pour un moteur coupleux avec un confort sans compromis.',
            'prix' => '550 500 XPF TTC',
            'image_force1' => 'https://dl.airtable.com/.attachmentThumbnails/ebae4d0ebedfba7cae586a70e3d0c9d7/11bfbd83',
            'force1' => 'Moteur Blue Core EURO5 125cc : En ville, sur la RDO ou en route de montagne, le nouveau NMAX 125 vous assure performances, plaisir et économie. Le moteur BLUE CORE, conforme à la norme EURO5, offre une forte accélération et consomme très peu de carburant, tandis que le réservoir de 7.1 litres vous permet de parcourir environ 300km entre chaque plein',
            'image_force2' => 'https://dl.airtable.com/.attachmentThumbnails/8d3d8731bdaae56149ac5031fbde976b/970a39cb',
            'force2' => 'Les technologies de série : Le NMAX est l’un des scooters les plus sophistiqués de sa catégorie. Equipé du système Smart Key de Yamaha, il démarre sans que vous ayez à chercher vos clefs dans vos poches ou dans votre sac. La nouvelle technologie Start & Stop s’adapte à votre conduite afin de réduire votre emprunte carbone et minimise la consommation de carburant. Vous pouvez aussi connecter votre smartphone à votre scooter pour vous donner des informations complémentaires sur l’engin et recevoir des notifications via l’application « MyRide ».',
            'image_force3' => 'https://dl.airtable.com/.attachmentThumbnails/979f92ca76f57d89f10a4d96d5bf4efb/a05aa5c7',
            'force3' => 'Freins : Le système ABS était déjà de série sur la version précédente du NMAX. Ce système couplé aux freins à disques avant et arrière offre un contrôle accru de votre engin sur toutes les routes. Désormais le NMAX est aussi équipé du TCS de série. Ce système de contrôle de la traction offre une conduite plus sûre sur les routes glissantes. Chez Yamaha votre sécurité est primordiale',
            'image_force4' => 'https://dl.airtable.com/.attachmentThumbnails/ac1ad34bf6b4a9e158dd170368763196/2f70570f',
            'force4' => 'Carénage et Cadre : Le nouveau design intègre un phare à LED double optique revu et des feux de positions intégrés, tandis que son nouveau feu arrière à LED et clignotants intégrés renforcent l’allure haut de gamme du scooter 125 ultime de Yamaha. La nouvelle conception du cadre contribue aux performances hors pair du NMAX. La répartition du poids est accrue, la position de conduite est plus confortable, la protection contre les éléments est améliorée et le bruit du moteur est réduit',
            'image_force5' => 'https://dl.airtable.com/.attachmentThumbnails/a31267b4a0216b4332d364eca9805974/828a684a',
            'force5' => 'Le confort à bord : Le NMAX 125 de Yamaha ne vous offre que le max. La position de conduite est confortable et sûre. La selle offre une assise agréable au pilote et au passager. Le rangement sous la selle est large. Vous pouvez en option rajouter un top case pour augmenter l’espace de rangement. Le Nmax est le partenaire pour le travail comme pour les loisirs.',
            'caracteristique_1' => 
                '<li>Type : Quatre soupapes, Quatre temps, Refroidissement liquide, simple arbre à cames en tête, Euro 5
                </li><li>Cylindrée : 125cc
                </li><li>Alésage X Course : 52,0 × 58,7 mm
                </li><li>Taux de compression : 11.2 :1
                </li><li>Couple maximal : 11,2nM (1,1kgf.m) à 6000tr/min
                </li><li>Puissance maximale : 9,0kW (12,2ch) à 8000tr/min
                </li><li>Système d’alimentation : injection électronique
                </li><li>Lubrification : Carter Humide
                </li><li>Allumage : Allumage électronique (TCI)
                </li><li>Mise en route : démarreur électrique
                </li><li>Transmission : Automatique à courroie trapézoïdale
                </li><li>Consommation de carburant : 2,2L/100km
                </li><li>Emission CO2 : 52g/km</li>',
            'caracteristique_2' => 
                '<li>Longueur hors tout : 1.935mm
                </li><li>Largeur hors tout : 740mm
                </li><li>Hauteur hors tout : 1.160mm
                </li><li>Hauteur de selle : 765mm
                </li><li>Empattement : 1.340mm
                </li><li>Garde au sol minimale : 125mm
                </li><li>Poids tous pleins faits : 131kg
                </li><li>Capacité réservoir d’essence : 7,1L</li>',
            'caracteristique_3' => 
                '<li>Suspension avant : fourche télescopique
                </li><li>Débattement avant : 100mm
                </li><li>Suspension arrière : moteur oscillant
                </li><li>Débattement arrière : 85mm
                </li><li>Frein avant : Disque simple à commande hydraulique
                </li><li>Frein arrière : Disque simple à commande hydraulique
                </li><li>Pneu avant : 110/70-13M/C 48P Tubeless
                </li><li>Pneu arrière : 130/70-13M/C 63P Tubeless</li>',
            ],
            [
            'product_type_id' => 1,
            'name' => 'D’ELIGHT 125 2021',
            'video' => 'YrO8Vzy7Gm0',
            'image' => 'https://dl.airtable.com/.attachmentThumbnails/9d9a66be34942cdead577541bac02875/89d96eee',
            'titre' => 'D’ELIGHT 125 2021',
            'petit_titre' => 'MOVE LIGHT',
            'description' => 'Donner l’accès à la qualité, c’est ce que propose Yamaha avec le scooter urbain D’ELIGHT 125. Avec son poids plume, il se faufile en ville grâce à ses dimensions compactes. Son moteur silencieux et économique doté d’une fonction « Start & Stop » automatique vous propulse à travers les embouteillages avec discrétion et assertivité. Arborant le badge Yamaha synonyme de fiabilité et profitant d’un tarif très compétitif, choisissez le D’ELIGHT l’esprit léger.',
            'prix' => '330 000 XPF TTC',
            'image_force1' => 'https://dl.airtable.com/.attachmentThumbnails/c297a559689162369c07e4b4789640bb/25a270da',
            'force1' => 'Moteur Blue Core EURO5 de 125cc : Cette dernière génération de moteur Yamaha Blue Core EURO5 de 1125cc profite d’une technologie qui garantit une efficacité maximale et une fiabilité hors pair. Son couple maximal s’exprime à des régimes plus bas que sur les autres modèles de la catégorie, ce qui garantit des économies de carburant et des accélérations convaincantes.',
            'image_force2' => 'https://dl.airtable.com/.attachmentThumbnails/d37761a9abdb1b5a55c88314219f1da6/70cb1a8b',
            'force2' => 'La technologie « Start & Stop » : Réduire sa consommation de carburant tout en réduisant son emprunte carbone. C’est le duo gagnant de cette technologie qui équipe ce scooter à petit prix. Ce système peut aussi être coupé si le pilote en ressent le besoin.',
            'image_force3' => 'https://dl.airtable.com/.attachmentThumbnails/cddf420ec343e21a11899ab1c2ef8919/01ee0c2c',
            'force3' => 'Espace de rangement généreux sous la selle : Le D’ELIGHT est doté d’un généreux espace de rangement sous la selle. Ce modèle est l’un des seuls scooters de sa catégorie dont le rangement peut accueillir un casque intégral. Vous pouvez aussi ajouter un top case pour ajouter une espace de rangement plus grand.',
            'image_force4' => 'https://dl.airtable.com/.attachmentThumbnails/8fa7a16e62e6d8a83b1ca98dfc669ad4/417feb29',
            'force4' => 'Le plus léger de sa catégorie : Comme son nom l’indique, le D’ELIGHT est léger. Pesant seulement 101kg tous pleins faits, il est le scooter le plus léger de sa catégorie. Cela lui confère une conduite agréable, une agilité hors paire dans les embouteillages et une adaptabilité aussi bien aux femmes qu’aux hommes.',
            'image_force5' => 'https://dl.airtable.com/.attachmentThumbnails/13563ae20070805ae3ad3985fff5a3d0/097a999b',
            'force5' => 'Design : Être vu en milieu urbain de jour ou de nuit c’est un gage de sécurité, surtout pour un scooter. Les grandes veilleuses avant du D’ELIGHT lui donnent cet avantage. Son plancher plat offre un confort et une adaptabilité pour une conduite en milieu urbain et pour les îles. Compact et léger, il est aussi doté d’un compteur analogique avec écran LCD et témoins d’informations lumineuses.',
            'caracteristique_1' => 
                '<li>Type : EURO 5, Simple arbre à cames en tête, Quatre Temps, Refroidissement par air, Deux soupapes.
                </li><li>Cylindrée : 125cc
                </li><li>Alésage X Course : 52,4 × 57,9 mm
                </li><li>Taux de compression : 10.2:1
                </li><li>Couple maximal : 9,8 Nm / 5 000 tr/min
                </li><li>Puissance maximale : 6,2 kW(8,4 ch) / 7 000 tr/min
                </li><li>Système d’alimentation : Injection électronique
                </li><li>Lubrification : Carter humide
                </li><li>Allumage : Allumage électronique (TCI)
                </li><li>Mise en route : Démarreur électrique
                </li><li>Transmission : Automatique à courroie trapézoïdale
                </li><li>Consommation de carburant : 1,8L/100km
                </li><li>Emission CO2 : 43g/km</li>',
            'caracteristique_2' => 
                '<li>Longueur hors tout : 1805mm
                </li><li>Largeur hors tout : 685mm
                </li><li>Hauteur hors tout : 1160mm
                </li><li>Hauteur de selle : 800mm
                </li><li>Empattement : 1275mm
                </li><li>Garde au sol minimale : 125mm
                </li><li>Poids tous pleins faits : 101kg
                </li><li>Capacité réservoir d’essence : 5,5L</li>',
            'caracteristique_3' => 
                '<li>Suspension avant : Fourche télescopique
                </li><li>Débattement avant : 
                </li><li>Suspension arrière : Moteur oscillant
                </li><li>Débattement arrière
                </li><li>Frein avant : Simple disque à commande hydraulique
                </li><li>Frein arrière : Frein à tambour
                </li><li>Pneu avant : 90/90-12 44J Tubeless
                </li><li>Pneu arrière : 100/90-10 56J Tubeless</li>',
            ],
        ];
        foreach($scooters AS $scooter):
            Scooter::create($scooter);
        endforeach;
    }
}
