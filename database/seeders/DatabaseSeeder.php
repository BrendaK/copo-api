<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            UserSeeder::class,
            CategorySeeder::class,
            ProductTypeSeeder::class,
            ProductSeeder::class,
            Moteur2TSeeder::class,
            MoteurEnduroSeeder::class,
            Moteur4TSeeder::class,
            Moteur4tFtSeeder::class,
            Moteur4tCommercialeSeeder::class,
            MotoSeeder::class,
            ScooterSeeder::class,
            WaverunnerSeeder::class,
            SavSeeder::class,
        ]);
    }
}
