<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Moto;

class MotoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $motos = [
            [
            'product_type_id' => 1,
            'name' => 'R7',
            'video' => 'AAzIcKknE2E',
            'image' => 'https://dl.airtable.com/.attachmentThumbnails/dedab1083932fee074b888926fcaf16e/09d4ca09',
            'titre' => 'R7',
            'petit_titre' => 'LÀ OÙ L’UNIVERS DE LA SÉRIE R RENCONTRE LE VÔTRE',
            'description' => 'La série R de Yamaha évolue. Alors que les modèles phares R1M, R1 et la R6 RACE titrée au Championnat du monde de Supersport continuent de dominer la catégorie, la R7 est spécifiquement destinée aux pilotes qui recherchent un design Supersport, des performances et un tarif accessible.',
            'prix' => '1 690 000 XPF TTC',
            'image_force1' => 'https://dl.airtable.com/.attachmentThumbnails/ab833f301db111296b7bfbacf0edee73/22db8412',
            'force1' => 'Moteur à haut rendement : le moteur bicylindre en configuration Crossplane de 689cc de la R7 offre une accélération remarquable et une réponse instantanée de l’accélérateur. L’allumage asynchrone des cylindres assure puissance et sensations dès que le régime s’élève. A l’allumage la sonorité du moteur atteste de son appartenance à la série R de Yamaha.',
            'image_force2' => 'https://dl.airtable.com/.attachmentThumbnails/841b58c8e124fcdf0bcf733c5f94235d/5dd32d68',
            'force2' => 'Embrayage A&S : La R7 est la première Yamaha à moteur CP2 équipée d’un embrayage antidrible assisté (A&S) qui offre un passage de rapports plus fluide en empêchant le surrégime moteur et le blocage de la roue arrière lors des freinages brusques. Le levier est plus souple, la maniabilité et le contrôle à l’approche des virages en est accru.',
            'image_force3' => 'https://dl.airtable.com/.attachmentThumbnails/8e28e8a2f8dd9873ccd94ce5422ff5da/408f7d68',
            'force3' => 'Coloris nouvelle génération : Le légendaire bleu Yamaha, aussi appelé Icon Blue, est bien présent sur cette supersport de nouvelle génération. Les côtés du carénage contrastent les parties supérieures de l’habillage, les panneaux inférieurs et les jantes. La R7 est aussi disponible en Black discret dévoilant peu de graphisme et de nuances.',
            'image_force4' => 'https://dl.airtable.com/.attachmentThumbnails/e817ff507e5da1126382db8225b796e9/25e857a7',
            'force4' => 'Suspensions entièrement réglables : Contrôle et précision hors pair. C’est ce que propose la R7 avec sa fourche inversée KYB de 41mm. Entièrement réglable en précharge, détente et compression, les suspensions avant s’adaptent à vous. Le nouveau système de suspension arrière à biellettes est doté d’un amortisseur réglable dont les caracteristiques d’amortissement et le tarage du ressort sont conçus pour s’adapter au caractère sportif de la R7.',
            'image_force5' => 'https://dl.airtable.com/.attachmentThumbnails/83ff6b995a9f4b377cf43c9871236dc1/e36d0871',
            'force5' => 'Carénage aérodynamique : Yamaha ne s’est pas contenté d’adapter un carénage à la R7. Celui-ci est complètement inédit. Les dimensions compactes du moteur CP2 ont donné aux concepteurs de développer un carénage athlétique plus fin que n’importe quel autre modèle de la série R. La face avant est incisive, les panneaux inférieurs sont en aluminium et la position de conduite est sportive et proche du châssis. La R7 fend littéralement l’air.',
            'caracteristique_1' => 
                '<li>Type : Deux cylindres, Refroidissement liquide, EURO5, Quatre temps, Quatre soupapes, Double arbre à cames en tête
                </li><li>Cylindrée : 689cc
                </li><li>Alésage X Course : 80,0 x 68,6 mm
                </li><li>Taux de compression : 11,5:1
                </li><li>Couple maximal : 67Nm @ 6500 tr/min
                </li><li>Puissance maximale : 54,0 kW (73,4 ch) à 8 750 tr/min
                </li><li>Système d’alimentation : injection électronique
                </li><li>Lubrification : Carter humide
                </li><li>Allumage : Allumage électronique (TCI)
                </li><li>Mise en route : Démarreur électrique
                </li><li>Transmission : Prise constante, Six vitesses
                </li><li>Transmission finale : Chaîne
                </li><li>Consommation de carburant : N/C</li>',
            'caracteristique_2' => 
                '<li>Longueur hors tout : 2070mm
                </li><li>Largeur hors tout : 705mm
                </li><li>Hauteur hors tout : 1160mm
                </li><li>Hauteur de selle : 835mm
                </li><li>Empattement : 1395mm
                </li><li>Garde au sol minimale : 135mm
                </li><li>Poids tous pleins faits : 188kg
                </li><li>Capacité réservoir d’essence : 13L
                </li><li>Capacité réservoir d’huile : 3,0L</li>',
            'caracteristique_3' => 
                '<li>Cadre : Diamant
                </li><li>Angle de chasse : 23°40’
                </li><li>Chasse : 90mm
                </li><li>Suspension avant : Fourche télescopique inversée
                </li><li>Débattement avant : 130mm
                </li><li>Suspension arrière : Bras oscillant, (biellettes de suspension)
                </li><li>Débattement arrière : 130mm
                </li><li>Frein avant : Double frein à disque de 298 mm de diamètre à commande hydraulique
                </li><li>Frein arrière : Frein à disque de 245 mm de diamètre à commande hydraulique
                </li><li>Pneu avant : 120/70ZR17M/C (58 W) Tubeless
                </li><li>Pneu arrière : 180/55ZR17M/C (73W) Tubeless</li>',
        ],
            [
            'product_type_id' => 1,
            'name' => 'MT-07 2021',
            'video' => 'Iyt2Z-n2eDQ',
            'image' => 'https://dl.airtable.com/.attachmentThumbnails/8b58594cc417c40e380341d5648cf17b/3a92f681',
            'titre' => 'MT-07 2021',
            'petit_titre' => 'L’ATTRAIT DU CÔTÉ OBSCUR',
            'description' => 'Son bicylindre en ligne de 689 cm³ de type Crossplane est conçu pour vous offrir un couple puissant et linéaire, qui garantit des accélérations fulgurantes et une économie de carburant importante. Nouveau guidon, nouveau design et nouveau moteur, la MT07 revient en force et assoie sa place de moto de choix pour les nouveaux pilotes, comme pour les plus aguerris. Elle est le roadster polyvalent par excellence.',
            'prix' => '1 590 000 XPF TTC',
            'image_force1' => '',
            'force1' => 'Moteur bicylindre 689cc Crossplane : La technologie du moteur Crossplane de YAMAHA a été développée pour la mythique YZF-R1. L’allumage asynchrone des cylindres couplé au vilebrequin calé à 270° offre une accélération et une traction impressionnante, tandis que le couple homogène élevé assure des performances exceptionnelles',
            'image_force2' => '',
            'force2' => 'Design nouvelle génération : Plus mature et organique, le design de la MT met l’accent sur le caractère HYPER NAKED, arborant un son compact, un éclairage intégré et un design à la pureté réaffirmée. La finition a été revue pour donner un aspect plus homogène et qualitatif. Les nouveaux coloris et motifs inédits de cette génération propulse la MT07 vers le futur',
            'image_force3' => '',
            'force3' => 'Réactive, abordable et économique : Avec son moteur à refroidissement liquide, son cadre léger et son design sportif, la MT-07 est un roadster particulièrement polyvalent, associant agilité, prix abordable et une consommation d’essence très basse. La moto idéale pour les pilotes novices ou expérimentés et adaptée aux routes de notre Fenua',
            'image_force4' => '',
            'force4' => 'Des nouveautés à tous les niveaux : Le nouveau guidon conique en aluminium est étendu de 15mm de chaque côté, affirmant ainsi la position de conduite et facilite le pilotage dans les virages. Des nouveaux disques de frein avant de 298mm de diamètre, associés à des étriers à 4 pistons, offrent un freinage plus progressif sans ajouter de poids. Un nouvel écran LCD, un nouveau feu avant, des clignotants à LED compacts et des nouveaux leviers… La MT07 monte en gamme et garde son rapport qualité-prix imbattable dans sa catégorie',
            'image_force5' => '',
            'force5' => '',
            'caracteristique_1' => 
                '<li>Type de moteur : Deux cylindres, quatre temps, refroidissement liquide, double arbre à cames en tête, quatre soupapes
                </li><li>Cylindrée : 689 cm³
                </li><li>Alésage x course : 80.0 x 68.6 mm
                </li><li>Taux de compression : 11.5 : 1
                </li><li>Puissance maximale : 54.0kW (73.4ch) à 8,750 tr/min
                </li><li>Couple maximal : 67.0N.m (6.8m.kgf) à 6,500tr/min
                </li><li>Lubrification : Carter humide
                </li><li>Embrayage : À bain d’huile, Multidisque
                </li><li>Allumage : Allumage électronique (TCI)
                </li><li>Mise en route : Démarreur électrique
                </li><li>Transmission : Prise constante, Six vitesses
                </li><li>Transmission finale : Chaîne
                </li><li>Consommation de carburant : 4.2l/100 km
                </li><li>Émission de CO₂ : 98g/km</li>',
            'caracteristique_2' => 
                '<li>Longueur hors tout : 2.085 mm
                </li><li>Largeur hors tout : 780 mm
                </li><li>Hauteur hors tout : 1.105 mm 
                </li><li>Hauteur de selle : 805 mm 
                </li><li>Empattement : 1.400 mm
                </li><li>Garde au sol minimale : 140 mm
                </li><li>Poids tous pleins faits : 184 kg 
                </li><li>Capacité du réservoir d’essence : 14L
                </li><li>Capacité du réservoir d’huile : 3.0L</li>',
            'caracteristique_3' => 
                '<li>Cadre : Diamant
                </li><li>Débattement avant : 130 mm
                </li><li>Angle de chasse : 24º50
                </li><li>Chasse : 90mm
                </li><li>Suspension avant : Fourche télescopique
                </li><li>Suspension arrière : Bras oscillant, (suspension à biellettes)
                </li><li>Débattement arrière : 130 mm
                </li><li>Frein avant : Double disque de 298 mm de diamètre à commande hydraulique
                </li><li>Frein arrière : Simple disque, Ø245 mm 
                </li><li>Pneu avant : 120/70 ZR 17M/C(58W) (Tubeless)
                </li><li>Pneu arrière : 180/55 ZR 17M/C(73W) (Tubeless)</li>',
            ],
            [
            'product_type_id' => 1,
            'name' => 'MT-09 2021',
            'video' => 'TUQNl45Hzbo',
            'image' => 'https://dl.airtable.com/.attachmentThumbnails/d1ec13ffab4087b6de302c2b02725d8b/b12887bd',
            'titre' => 'MT-09 2021',
            'petit_titre' => 'UNE ICÔNE EN PLEINE RÉVOLUTION',
            'description' => 'Plus puissante, plus légère, plus coupleuse… la MT09 évolue et assoie sa position de Master of Torque. Sa réputation n’est plus à faire auprès des motards du Fenua. Non content de simplement adapter le moteur à la nouvel norme EURO5, Yamaha donne à sa 3 cylindres un nouveau moteur plus performant et plus léger, des nouvelles jantes, une suspension arrière corrigée et un design complètement revu. L’impétueuse garde ses atouts des dernières générations et se projette dans le futur en ne faisant rien pour passer inaperçue.',
            'prix' => '1 690 000 XPF TTC',
            'image_force1' => 'https://dl.airtable.com/.attachmentThumbnails/87a3c44a398318a1dd125dfe374b96cf/77ceec22',
            'force1' => 'Maniable et agile : Le châssis compact en aluminium et le bras oscillant utilisant le moteur CP3 comme élément rigidificateur octroient à la MT-09 une agilité sans égale. Le centrage des masses est optimisé, son châssis est léger. Plus rigide et plus compact qu’auparavant, le châssis donne à cette nouvelle génération un rapport poids puissance amélioré.',
            'image_force2' => 'https://dl.airtable.com/.attachmentThumbnails/8f80655c93ef2b9c9bca920ee0be6833/3ee117f1',
            'force2' => 'Moteur CP3 à couple élevé : Le moteur CP3 de 890 cm³ a fait la réputation de la MT-09 dès sa sortie en 2013. Son caractère coupleux l’a très vite placée au sommet de sa catégorie. Plus coupleux, plus puissant, le moteur EURO5 de la MT09 est plus économique et moins polluant que la génération précédente. La configuration CROSSPLANE est optimisée pour des accélérations hors-normes et une sonorité unique.',
            'image_force3' => 'https://dl.airtable.com/.attachmentThumbnails/99804b2bd2056e21de03d939ccc8b35a/d51b6c1d',
            'force3' => 'Shifter et A&S : Le shifter hérité de la YZF-R1 permet de monter plus rapidement les rapports et avec plus de fluidité afin d’exploiter pleinement le couple brut et homogène de la MT-09. L’embrayage antidribble assisté (A&S) permet, quant à lui, de fournir une stabilité renforcée du châssis lors des rétrogradages rapides.',
            'image_force4' => 'https://dl.airtable.com/.attachmentThumbnails/5f92cbc78a9552b3aab71a2184012585/f1ee5c44',
            'force4' => 'Fourche entièrement réglable : La fourche inversée de 41mm de la MT-09 a été sensiblement améliorée avec l’ajout du réglage de la compression. Le pilote a donc un éventail de réglages plus large et de meilleures performances de suspension. La compression se trouve sur le fourreau droit et la détente sur le fourreau gauche.',
            'image_force5' => 'https://dl.airtable.com/.attachmentThumbnails/1150fc91684102922c01f676249d6afd/0b5146c9',
            'force5' => 'Systèmes de contrôle électroniques : L’unité de mesure d’inertie (IMU), développée à partir de la YZF-R1, gère les aides au pilotage. Le contrôle de la traction (TCS) à 3 modes sensibles à l’inclinaison, un système de glisse (SCS), du décollage de la roue avant (LIF) et de contrôle de freins (BC) sont gérés par ordinateur et garantissent un contrôle ultime dans toutes les conditions météo. L’ensemble est affiché sur un nouvel écran TFT couleur de 3,5 pouces.',
            'caracteristique_1' => 
                '<li>Type de moteur : Trois cylindres, quatre temps, refroidissement liquide, double arbre à cames en tête, quatre soupapes
                </li><li>Cylindrée : 890 cm³
                </li><li>Alésage x course : 78.0 x 62.1 mm
                </li><li>Taux de compression : 11.5 : 1
                </li><li>Puissance maximale : 87.5kW (119ch) à 10,000 tr/min
                </li><li>Couple maximal : 93.0N.m (9.5m.kgf) à 7,000tr/min
                </li><li>Lubrification : Carter humide
                </li><li>Embrayage : À bain d’huile, Multidisque
                </li><li>Allumage : Allumage électronique (TCI)
                </li><li>Mise en route : Démarreur électrique
                </li><li>Transmission : Prise constante, Six vitesses
                </li><li>Transmission finale : Chaîne
                </li><li>Consommation de carburant : 5.0L/100 km
                </li><li>Émission de CO₂ : 116g/km</li>',
            'caracteristique_2' => 
                '<li>Longueur hors tout : 2.090 mm
                </li><li>Largeur hors tout : 795 mm
                </li><li>Hauteur hors tout : 1.190 mm 
                </li><li>Hauteur de selle : 825 mm 
                </li><li>Empattement : 1.430 mm
                </li><li>Garde au sol minimale : 140 mm
                </li><li>Poids tous pleins faits : 189 kg 
                </li><li>Capacité du réservoir d’essence : 14L
                </li><li>Capacité du réservoir d’huile : 3.50L</li>',
            'caracteristique_3' => 
                '<li>Cadre : Diamant
                </li><li>Débattement avant : 130 mm
                </li><li>Angle de chasse : 25º
                </li><li>Chasse : 108mm
                </li><li>Suspension avant : Fourche télescopique
                </li><li>Suspension arrière : Bras oscillant, (suspension à biellettes)
                </li><li>Débattement arrière : 122 mm
                </li><li>Frein avant : Double disque, Ø298 mm 
                </li><li>Frein arrière : Simple disque, Ø245 mm 
                </li><li>Pneu avant : 120/70ZR17M/C (58W) (Tubeless)
                </li><li>Pneu arrière : 180/55ZR17M/C (73W) (Tubeless)</li>',
            ],
            [
            'product_type_id' => 1,
            'name' => 'MT-09SP 2021',
            'video' => 'mHju1ynuzeU',
            'image' => 'https://dl.airtable.com/.attachmentThumbnails/c08e46f694d789b21169cf8713f3a04d/3d9eff3d',
            'titre' => 'MT-09SP 2021',
            'petit_titre' => 'DÉFIEZ LE CÔTÉ OBSCUR',
            'description' => 'Le Dark Side of Japan ne dort jamais. Chaque Yamaha MT s’inspire de cette philosophie. Et la MT-09 SP, machine exclusive, est le dernier chef-d’œuvre à émerger des ténèbres. Arborant des coloris exclusifs inspirés de la R1M, et équipée de suspensions haut de gamme et d’un régulateur de vitesse, l’Hyper Naked trois cylindres est prête à déclencher les passions.',
            'prix' => '1 790 000 XPF TTC',
            'image_force1' => 'https://dl.airtable.com/.attachmentThumbnails/15547fab2bce2b29e7beb34d3eaec686/5379b858',
            'force1' => 'Coloris SP dédiés : Les couleurs dédiés de la SP rappellent la finition Icon Performance exclusive de la R1M, la mythique moto de compétition GP. Les leviers, le guidon, le pignon d’entrainement, la fourche avant ainsi que de nombreux autres composants arborent un revêtement DLC noir.',
            'image_force2' => 'https://dl.airtable.com/.attachmentThumbnails/1b1e187875fb0f743b1830bfa9dbd091/71234f81',
            'force2' => 'Suspensions entièrement réglables : Sa fourche KYB, entièrement réglable, est dotée d’un amortisseur en compression qui permet de choisir des réglages adaptés à toutes les allures. Les tubes de 41mm profitent d’un revêtement DLC offrant d’excellentes propriétés de glissement et un rendu de grande qualité. L’amortisseur OHLÏNS à l’arrière garanti une qualité haut de gamme.',
            'image_force3' => 'https://dl.airtable.com/.attachmentThumbnails/6982866dddaaf4eb6ba3f6c3d21a1079/226527c4',
            'force3' => 'Selle à double surpiqûre : Ne faire qu’un avec la machine. C’est la sensation recherchée dans le design des composant de la MT09 SP. Le design de la selle et d’autres composants, comme le réservoir, ont été optimisés par le processus Kanno Hyoka (qui évalue les performances de la moto en fonction des réactions du pilote d’essai). Les coutures contrastées et élégantes de la selle renforcent l’allure de la moto et procurent un confort non négligeable.',
            'image_force4' => 'https://dl.airtable.com/.attachmentThumbnails/534ea17a09972f7f3dfb06e297d53c26/1284c27d',
            'force4' => 'Bras oscillant en aluminium brossé : La finition du bras oscillant en aluminium brossé anodisé souligne l’allure haut de gamme. Au même titre que la finition Crystal Graphite du cadre, elle fait ressortir la beauté pure du châssis compact de la MT09 SP.',
            'image_force5' => 'https://dl.airtable.com/.attachmentThumbnails/a0e509e4b77ded8dec3187494088aaac/7783b1c6',
            'force5' => 'Jantes Spinforged légères : Pesant 700g de moins que le modèle précédent, les nouvelles jantes Spinforged sont les plus légères jamais montées sur une Yamaha de production. Elles offrent une agilité remarquable en diminuant l’inertie à l’arrière de 11%. Cette réduction du poids non suspendu améliore également les performances des suspensions et contribue ç une réduction impressionnante de 9% de consommation de carburant.',
            'caracteristique_1' => 
                '<li>Type de moteur : Trois cylindres, quatre temps, refroidissement liquide, double arbre à cames en tête, quatre soupapes
                </li><li>Cylindrée : 890 cm³
                </li><li>Alésage x course : 78.0 x 62.1 mm
                </li><li>Taux de compression : 11.5 : 1
                </li><li>Puissance maximale : 87.5kW (119ch) à 10,000 tr/min
                </li><li>Couple maximal : 93.0N.m (9.5m.kgf) à 7,000tr/min
                </li><li>Lubrification : Carter humide
                </li><li>Embrayage : À bain d’huile, Multidisque
                </li><li>Allumage : Allumage électronique (TCI)
                </li><li>Mise en route : Démarreur électrique
                </li><li>Transmission : Prise constante, Six vitesses
                </li><li>Transmission finale : Chaîne
                </li><li>Consommation de carburant : 5.0L/100 km
                </li><li>Émission de CO₂ : 116g/km</li>',
            'caracteristique_2' => 
                '<li>Longueur hors tout : 2,090 mm
                </li><li>Largeur hors tout : 795 mm
                </li><li>Hauteur hors tout : 1,190 mm 
                </li><li>Hauteur de selle : 825 mm 
                </li><li>Empattement : 1,430 mm
                </li><li>Garde au sol minimale : 140 mm
                </li><li>Poids tous pleins faits : 189 kg 
                </li><li>Capacité du réservoir d’essence : 14L
                </li><li>Capacité du réservoir d’huile : 3.50L</li>',
            'caracteristique_3' => 
                '<li>Cadre : Diamant
                </li><li>Débattement avant : 130 mm
                </li><li>Angle de chasse : 25º
                </li><li>Chasse : 108mm
                </li><li>Suspension avant : Fourche télescopique
                </li><li>Suspension arrière : Bras oscillant, (suspension à biellettes)
                </li><li>Débattement arrière : 122 mm
                </li><li>Frein avant : Double disque, Ø298 mm 
                </li><li>Frein arrière : Simple disque, Ø245 mm 
                </li><li>Pneu avant : 120/70ZR17M/C (58W) (Tubeless)
                </li><li>Pneu arrière : 180/55ZR17M/C (73W) (Tubeless)</li>',
            ],
            [
            'product_type_id' => 1,
            'name' => 'Tracer 900',
            'video' => 'jQ-oWDDLL4Y',
            'image' => 'https://dl.airtable.com/.attachmentThumbnails/f60e988c8a928012eae9c64db63a3196/be1f0aae',
            'titre' => 'Tracer 900',
            'petit_titre' => 'IMAGINEZ DE NOUVEAUX LENDEMAINS',
            'description' => 'La routière sportive la plus vendue d’Europe, entièrement repensée, est équipée d’un moteur trois cylindres de 890 cm³ encore plus généreux, produisant un couple encore plus impressionnant et des accélérations enivrantes. Que vous sillonniez des routes de montagne ou que vous parcouriez de grandes distances, son nouveau châssis léger garantit une agilité sportive et une stabilité routière, en solo comme en duo.',
            'prix' => '1 990 000 XPF TTC',
            'image_force1' => '',
            'force1' => 'La TRACER 900 en chiffres : Son moteur 3 cylindres CROSSPLANE de 890cc produit 93Nm à 7000tr/min, avec une puissance maximale de 119ch à 10.000tr/min. Poids tout pleins faits de 213kg, capacité de chargement de 193kg, capacité de réservoir de 19L et une autonomie supérieure à 350km.',
            'image_force2' => '',
            'force2' => 'Moteur CP3 de 890cc EURO5 : Le nouveau moteur CP3 à refroidissement liquide de 890cc développe un couple plus élevé à bas régime. Le résultat, le moteur produit des accélérations remarquables et offre des vitesses de sortie de virage époustouflantes, ce qui en fait la TRACER 900 la plus sportive de tous les temps. La répartition des rapports optimisée et l’embrayage antidrible affiné offrent des performances exceptionnelles. La ligne d’échappement à double sortie est nouvelle, l’admission repensée et le couple moteur est amélioré. Le moteur a réellement évolué.',
            'image_force3' => '',
            'force3' => 'Nouveau cadre en aluminium : La TRACER 900 nouvelle génération est équipée d’un nouveau cadre Deltabox en aluminium coulé sous pression offrant une rigidité plus élevée et donc une stabilité de la moto améliorée. Le bras oscillant a été rallongé de 64mm pour garantir une stabilité à grande vitesse. Le poids a été réduit, le moteur est plus puissant et la stabilité renforcée. La nouvelle TRACER 900 a de quoi séduire.',
            'image_force4' => '',
            'force4' => 'Centrale inertielle (IMU) à six axes : Faire de chaque trajet un plaisir tout en garantissant un contrôle total de votre TRACER 900 dans toutes les conditions de route et de météo, c’est ce que propose Yamaha avec cette nouvelle génération. Développée à partir de la R1M, la nouvelle centrale inertielle (IMU) compacte à six axes gère un système de contrôle de la traction (TCS) à 3 modes sensible à l’inclinaison, un système de contrôle de la glisse (SCS), du décollage de la roue avant (LIF) et de contrôle des freins (BC).',
            'image_force5' => '',
            'force5' => 'Le confort à bord : La nouvelle TRACER 900 vient équipée de 2 écrans TFT de 3,5 pouces qui affiche des données intuitives et faciles à lire. La selle est réglable sur 2 positions vous permettant d’ajuster la hauteur sans outil et de personnaliser l’ergonomie de votre style de conduite et de votre morphologie. La bulle haute assure une meilleure protection contre le vent et est facilement réglable. Les suspensions avant et arrière sont aussi entièrement réglables pour s’adapter à vous.',
            'caracteristique_1' => 
                '<li>Type de moteur : Trois cylindres, quatre temps, refroidissement liquide, double arbre à cames en tête, quatre soupapes
                </li><li>Cylindrée : 890 cm³
                </li><li>Alésage x course : 78.0 x 62.1 mm
                </li><li>Taux de compression : 11.5 : 1
                </li><li>Puissance maximale : 87,5 kW (119 ch) à 10 000 tr/min
                </li><li>Couple maximal : 93,0 Nm (9,5 m.kgf) à 7 000 tr/min
                </li><li>Lubrification : Carter humide
                </li><li>Embrayage : À bain d’huile, Multidisque
                </li><li>Allumage : Allumage électronique (TCI)
                </li><li>Mise en route : Démarreur électrique
                </li><li>Transmission : Prise constante, Six vitesses
                </li><li>Transmission finale : Chaîne
                </li><li>Consommation de carburant : 5.0l/100 km
                </li><li>Émission de CO₂ : 116g/km</li>',
            'caracteristique_2' => 
                '<li>Longueur hors tout : 2,175 mm
                </li><li>Largeur hors tout : 885 mm
                </li><li>Hauteur hors tout : 1,430 mm max 1,470 mm
                </li><li>Hauteur de selle : 810 mm max 825 mm
                </li><li>Empattement : 1,500 mm
                </li><li>Garde au sol minimale : 135 mm
                </li><li>Poids tous pleins faits : 213 kg
                </li><li>Capacité du réservoir d’essence : 18L
                </li><li>Capacité du réservoir d’huile : 3.5L</li>',
            'caracteristique_3' => 
                '<li>Cadre : Diamant
                </li><li>Débattement avant : 130 mm
                </li><li>Angle de chasse : 25º
                </li><li>Chasse : 108mm
                </li><li>Suspension avant : Fourche télescopique
                </li><li>Suspension arrière : Bras oscillant, (suspension à biellettes)
                </li><li>Débattement arrière : 137 mm
                </li><li>Frein avant : Double disque, Ø298 mm
                </li><li>Frein arrière : Simple disque, Ø245 mm
                </li><li>Pneu avant : 120/70ZR17M/C (58W)
                </li><li>Pneu arrière : 180/55ZR17M/C (73W)</li>',
            ]
        ];
        foreach($motos AS $moto):
            Moto::create($moto);
        endforeach;
    }
}
