<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Sav;

class SavSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $savs = [
        [
        "name" =>"Filtres à air",
        "image" => "https://dl.airtable.com/.attachmentThumbnails/386bf6a62d36feddbdae5789fe85a5b1/12d4dc5d",
        "description" =>"En empêchant la poussière et les impuretés de s’introduire dans le moteur, le filtre à air est un composant majeur de votre Yamaha.

        Même si les filtres à air adaptables peuvent sembler très proches d’un filtre à air d’origine Yamaha, ils sont en réalité très différents.
        
        L’utilisation d’un filtre à air inadapté peut engendrer une perte de puissance ainsi qu’une usure prématurée du moteur : commander un filtre à air d’origine chez votre concessionnaire Yamaha vous garantit une filtration parfaite.
        
        Les filtres à air d’origine Yamaha sont conçus de façon à maintenir un débit d’air optimal en fonction de la puissance, de la cylindrée, du nombre de soupapes et du système d’alimentation en essence de votre Yamaha. Ils sont prévus pour empêcher l’intrusion d’éventuels corps étrangers dans le moteur et pour maintenir le bon débit d’air en toute circonstance. Les filtres adaptables qui peuvent être disponibles sur le marché, peuvent s’encrasser rapidement et réduire sensiblement le flux d’air.
        
        Même si ce n’est pas visible à l’œil nu, le filtre à air permet à votre Yamaha d’être alimenté par de l’air propre et garantir un fonctionnement optimal de son moteur. Lorsqu’il sera temps de remplacer votre filtre à air, n’hésitez pas une seconde ! Utilisez un filtre à air d’origine Yamaha : vous serez certain qu’il est parfaitement adapté à votre machine et à l’environnement dans lequel elle évolue.",
        ],
        [
        "name" =>"Filtres à huile",
        "image" => "https://dl.airtable.com/.attachmentThumbnails/a225010009c271419f4ced68f95ef126/dcb540f1",
        "description" =>"Le filtre à huile nettoie l’huile (moteur) qui circule à travers le moteur. Si le filtre à huile n’est pas changé de façon périodique il peut se boucher avec des particules (débris) obligeant l’huile à circuler dans le moteur sans passer par le filtre, ayant pour conséquence la réduction de la durée de vie du moteur.

        Si l’on compare la durée de vie de plusieurs filtres à huile, on constate que le filtre à huile d’origine Yamaha dure plus longtemps que n’importe quel autre filtre à huile. Les filtres à huile d’origine sont parfaitement adaptés aux intervalles de maintenance définis par Yamaha. Entre deux révisions, un filtre à huile adaptable sera plus saturé qu’un filtre à huile d’origine.",
        ],
        [
        "name" =>"Batterie",
        "image" => "https://dl.airtable.com/.attachmentThumbnails/2741fed9dd63faaf8a08f1db0104d28c/9d425c62",
        "description" =>"Comme la plupart des gens, vous considérez comme normal que votre batterie fonctionne correctement tout au long de l’année. Les batteries modernes sont fiables et ne nécessitent que de très peu d’entretien, mais elles n’aiment pas les longues périodes d’inactivités.

 
        Si votre Yamaha n’est pas utilisé régulièrement, nous vous recommandons donc de connecter votre batterie à notre chargeur YEC-50. Développé en collaboration avec la société CTEK, ce chargeur électrique propose un mode spécial qui maintient votre batterie en parfaite condition. Les différentes phases de charge de ce chargeur peuvent aussi ressusciter de veilles batteries.
        
        Pour garder un œil sur le niveau de charge de votre batterie, vous pouvez y connecter un indicateur de charge. Le chargeur est livré d’origine avec un faisceau équipé d’un connecteur qui permet de brancher facilement le chargeur à votre batterie.",
        ],
        [
        "name" =>"Plaquette de frein",
        "image" => "https://dl.airtable.com/.attachmentThumbnails/4b8ce8907f4664c0fdf3c59d255b042b/2e6d20d2",
        "description" =>"Prenez quelques secondes pour imaginer ce que subissent vos plaquettes de frein à chacune de vos sorties… À chaque fois que vous freinez les plaquettes sont écrasées par les pistons de la pince de frein sur la surface du disque : elles subissent une pression extrême et stoppent votre machine en quelques secondes grâce à leur coefficient de friction élevé.
 
        La plupart des motos et scooters peuvent s’arrêter plus rapidement qu’ils ne peuvent accélérer. Ces extraordinaires performances du frein à disque sont assurées par une garniture de quelques centimètres carrés collée sur une plaque de pression.
        
        Les plaquettes de frein d’origine Yamaha sont développées spécifiquement pour chaque modèle de la gamme afin d’atteindre des performances optimales de freinage et d’équilibre en fonction des caractéristiques du modèle. À l’inverse, les plaquettes de freins adaptables sont développées pour se monter sur le plus grand nombre de machines, sans prendre en considération leur puissance, leur poids ou leur utilisation.
        
        Les plaquettes de frein d’origine Yamaha bénéficient d’un procédé de collage unique qui garantit que la garniture reste solidaire de la plaque de pression lorsqu’elles sont soumises à de très fortes pressions et à des températures extrêmes. Le matériau de la garniture sans amiante est constitué de fibres métalliques et de fibres résistants à la chaleur afin de garantir un freinage puissant et constant. De son côté, la plaque de pression reçoit un traitement anticorrosion pour que le freinage soit régulier et efficace qu’il fasse chaud ou froid.
        
        Comme toutes les pièces d’origine Yamaha, nos plaquettes de frein sont réalisées en utilisant des matériaux de haute qualité et doivent subir une série de tests très stricts avant de recevoir la certification « pièce d’origine Yamaha ». Ces tests permettent de garantir que l’adhérence entre la plaque de pression et la garniture, leur durée de vie, leur tenue aux vibrations et leur comportement sur une large plage de température est supérieur aux plaquettes adaptables.",
        ],
        [
        "name" =>"Kit chaîne",
        "image" => "https://dl.airtable.com/.attachmentThumbnails/d7a0bcfa6c9c0fa397a3175db11a3988/3a345b8d",
        "description" =>"La chaine, le pignon et la couronne sont soumis à rude épreuve sur votre Yamaha. Ils doivent transmettre la puissance du moteur à la roue arrière, encaisser les accélérations ainsi que les décélérations générées par le frein moteur. Ces composants sont de plus exposés au froid, au chaud, à la pluie, aux gravillons, au sel et à la boue.

 
        Les kits chaines d’origine Yamaha doivent répondre aux mêmes standards de qualité que ceux qui sont montés en usine sur votre Yamaha. Ils minimisent ainsi les pertes de puissance qui pourraient être induites par la transmission finale et offrent une haute résistance à l’usure pour une durée de vie maximale
        
        Chaque chaine d’origine Yamaha a été développée pour résister à la puissance du moteur de votre moto et ses caractéristiques mécaniques s’accordent à la dureté du pignon et de la couronne d’origine. Comparé à un autre kit chaine, les composants Yamaha seront plus résistants, s’useront moins vite et vous permettront de parcourir plus de kilomètres sans le moindre problème.
        
         
        1. Axe 
        L’utilisation d’un alliage d’acier spécial et le traitement thermique poussé réduisent l’usure de la surface de l’axe et fournissent une parfaite résistance à la charge.
        
        2. Bague 
        L’état de surface extrêmement soigné des bagues conjugué à un traitement de surface permet à la chaîne de beaucoup mieux résister à l’allongement dû à l’usure.
        
        3. Graisse 
        Une graisse spéciale est insérée entre les axes et les bagues afin d’augmenter la durée de vie de la chaîne.
        
        4. Plaque de maillon 
        Les plaques de maillon sont à la fois légères et robustes. Elles subissent un grenaillage afin de mieux résister à la fatigue et aux risques de fissure engendrés par la chaleur.
        
        5. Rouleau 
        Les rouleaux en acier renforcé réduisent l’usure des dents de la couronne et du pignon.
        
        6. Joint torique
        Son profil spécial permet de diminuer les pertes dues aux frottements.",
        ],
        [
        "name" =>"Courroie de transmission",
        "image" => "https://dl.airtable.com/.attachmentThumbnails/5984aac1318f7fee0ddfa8f20d1b48e9/366995b3",
        "description" =>"Spécialement conçues pour correspondre aux caractéristiques du véhicule

        Les courroies d’origine Yamaha sont conçues pour délivrer un maximum de résistance et de durabilité et s’adapter aux véhicules sur lesquels elles doivent être installées. Elles utilisent les meilleurs matériaux disponibles sur le marché afin d’augmenter constamment la longévité.
        
        Si la qualité de la courroie est inférieure à celle d’origine, cela peut avoir des conséquences très dommageables, comme la rupture de la courroie, la perte de contrôle du véhicule ou la roue arrière bloquée.
        
        Conception des matériaux pour une longévité accrue
        
        Les courroies trapézoïdales Yamaha ont été soumises à des tests approfondis, définissant que le matériau, la longueur et la largeur de la courroie ont été spécifiquement choisis pour augmenter sa durée de vie. Le faible bord d’attaque de la courroie en combinaison avec une petite poulie offre une très bonne flexibilité et une grande résistance à la température.
        
        Les composants des courroies d’origine ont été développés dans le but d’apporter une grande résistance à l’abrasion et une longévité optimale.
        
        Développées spécifiquement pour chaque modèle
        
        Le système de transmission finale doit être dimensionné en fonction de la puissance développée par le moteur, ainsi chaque courroie de transmission doit être dimensionnée en fonction du véhicule. 
        
        Sur un scooter, la courroie doit être parfaitement ajustée au variateur : une courroie adaptable dont le profil n’est pas parfaitement adapté aux poulies du variateur peut limiter les performances du moteur.",
        ],
        [
        "name" =>"Bougie",
        "image" => "https://dl.airtable.com/.attachmentThumbnails/3f830b46a437722e62cf268aedb6395b/fdeabe62",
        "description" =>"Pour les bougies, il est très important que l’étincelle soit située au bon endroit dans la chambre de combustion afin d’assurer des performances optimales au moteur. Plusieurs facteurs entrent en ligne de compte : le nombre et la taille des conduits d’admission, ainsi que le nombre et l’angle des soupapes d’échappements.

        ll est extrêmement important d’utiliser les bougies appropriées pour chaque modèle de moto afin d’obtenir des performances optimales et ainsi éviter de sévères dommages au moteur.
        
        Il est extrêmement important d’utiliser les bougies appropriées pour chaque modèle de moto afin d’obtenir des performances optimales et ainsi éviter de sévères dommages au moteur.
        
        Respecter la durée de vie d’une bougie pour des performances optimales
        
        Même dans des conditions d’utilisation normale, les bougies doivent être remplacées régulièrement. Plus une bougie vieillie, plus la mise en route du moteur est difficile et plus la combustion du mélange air/essence se dégrade. Yamaha sélectionne les meilleures bougies pour chaque modèle de chaque gamme.",
        ],
    ];
        foreach($savs AS $sav):
            Sav::create($sav);
        endforeach;
    }
}
