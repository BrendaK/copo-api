<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\ProductType;

class ProductTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $productTypes = [
            [
                'name'=>'moto',
                'category_id'=> 1
            ],
            [
                'name'=>'scooter',
                'category_id'=> 1
            ],
            [
                'name'=>'moteur2temps',
                'category_id'=> 2
            ],
            [
                'name'=>'moteurEnduro',
                'category_id'=> 2
            ],
            [
                'name'=>'waverunner',
                'category_id'=> 2
            ],
            [
                'name'=>'casque',
                'category_id'=> 3
            ],
            [
                'name'=>'veste',
                'category_id'=> 3
            ],
            [
                'name'=>'tee-shirt',
                'category_id'=> 3
            ],
            [
                'name'=>'casquette',
                'category_id'=> 3
            ],
            [
                'name'=>'bâche',
                'category_id'=> 3
            ],
            [
                'name'=>'mug',
                'category_id'=> 3
            ],
        ];
        foreach($productTypes AS $productType):
            ProductType::create($productType);
        endforeach;
    }
}
