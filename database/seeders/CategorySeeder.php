<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Category;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = [
            ['name'=>'route'],
            ['name'=>'marine'],
            ['name'=>'vêtements et accessoires'],
        ];
        foreach($categories AS $category):
            Category::create($category);
        endforeach;
    }
}
