<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Moteur4tCommerciale;

class Moteur4tCommercialeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $moteur4tCommerciales = [
            [
                'product_type_id' => 2,
                'name' => 'F20CMHS/L',
                'image' => 'https://dl.airtable.com/.attachmentThumbnails/84ca4e9d246e52ea06102feeb3cac523/d20a5b83',
                'titre' => 'GAMME COMMERCIALE',
                'prix' => '450 000 XPF TTC',
                'caracteristique_1' => 
                    '<li>Type : 4 temps, 2 Cylindres en ligne, SOHC
                    </li><li>Cylindrée : 362cc
                    </li><li>Bougie : DPR6EB-9
                    </li><li>Alésage X Course : 63.0X58.1mm
                    </li><li>Taux de compression : 9.3 :1
                    </li><li>Plage de régime à plein gaz : 6000tr/minute
                    </li><li>Puissance maximale : 14.7kw(20cv)
                    </li><li>Système d’alimentation : 1 carburateur
                    </li><li>Lubrification : Carter humide
                    </li><li>Capacité en huile : 1.8litres
                    </li><li>Allumage : Electronique (CDI)
                    </li><li>Mise en route : Manuel avec prime Start™
                    </li><li>Consommation de carburant : 6,9L/h @5000tr/minute</li>',
                'caracteristique_2' => 
                    '<li>Poids avec hélice : 51-54kg
                    </li><li>Longueur d’arbre : Court (438mm) Long(565mm)
                    </li><li>Capacité réservoir d’essence : 24L</li>',
                'caracteristique_3' => 
                    '<li>TRIM : 4 positions
                    </li><li>Angle de virage : 45 degrés de chaque côté
                    </li><li>Contrôle : Barre franche
                    </li><li>Accessoires inclus : Nourrice essence, coupe circuit, hélice</li>',
                ],
                [
                'product_type_id' => 2,
                'name' => 'F75FETL',
                'image' => 'https://dl.airtable.com/.attachmentThumbnails/7a5f570ded08b85de4c66f0706539cf8/78261682',
                'titre' => 'GAMME COMMERCIALE',
                'prix' => '1 170 000 XPF TTC',
                'caracteristique_1' => 
                    '<li>Type : 4 temps, 4 cylindres en ligne, 16 soupapes, SOHC
                    </li><li>Cylindrée : 1832cc
                    </li><li>Bougie : LKR6E-9N
                    </li><li>Alésage X Course : 81.0X88.9mm
                    </li><li>Taux de compression : 10.0 :1
                    </li><li>Plage de régime à plein gaz : 5000 – 6000tr/minute
                    </li><li>Puissance maximale : 55.2kw (75cv)
                    </li><li>Système d’alimentation : Injection Electronique
                    </li><li>Lubrification : Carter humide
                    </li><li>Capacité en huile : 3.2 litres
                    </li><li>Allumage : Electronique (CDI)
                    </li><li>Mise en route : Électrique avec prime Start™
                    </li><li>Consommation de carburant : 28,7L/h @6000tr/minute</li>',
                'caracteristique_2' => 
                    '<li>Poids avec hélice : 162 kg
                    </li><li>Longueur d’arbre : Long(516mm) 
                    </li><li>Capacité réservoir d’essence : 24L</li>',
                'caracteristique_3' => 
                    '<li>TRIM : 5 positions
                    </li><li>Angle de virage : 35 degrés
                    </li><li>Contrôle : Commande à distance/barre franche en option
                    </li><li>Sortie de l’alternateur : 35Ah(Charge Batterie 21Ah)</li>',
                ],
                [
                'product_type_id' => 2,
                'name' => 'F100BETL/X',
                'image' => 'https://dl.airtable.com/.attachmentThumbnails/07744cb63214747b052c1a7a453cea6a/9f8e3938',
                'titre' => 'GAMME COMMERCIALE',
                'prix' => '1 350 000 XPF TTC',
                'caracteristique_1' => 
                    '<li>Type : 4 temps, 4 cylindres en ligne, 16 soupapes, DOHC
                    </li><li>Cylindrée : 1596cc
                    </li><li>Bougie : LFR5A-11
                    </li><li>Alésage X Course : 79.0X81.4mm
                    </li><li>Taux de compression : 8.9 :1
                    </li><li>Plage de régime à plein gaz : 5000 – 6000tr/minute
                    </li><li>Puissance maximale : 73.6kw(100cv)
                    </li><li>Système d’alimentation : 4 Carburateurs
                    </li><li>Lubrification : Carter humide
                    </li><li>Capacité en huile : 3.7 litres
                    </li><li>Allumage : Electronique (CDI)
                    </li><li>Mise en route : Électrique avec prime Start™
                    </li><li>Consommation de carburant : 31,9L/h @5500tr/minute</li>',
                'caracteristique_2' => 
                    '<li>Poids avec hélice : 183-188kg
                    </li><li>Longueur d’arbre : Long(516mm) X-Long(643mm)</li>',
                'caracteristique_3' => 
                    '<li>TRIM : Electrique variable
                    </li><li>Angle de virage : 30 degrés
                    </li><li>Contrôle : Commande à distance
                    </li><li>Sortie de l’alternateur : 20Ah</li>',
                ],
                [
                'product_type_id' => 2,
                'name' => 'F150FETX',
                'image' => 'https://dl.airtable.com/.attachmentThumbnails/064a910b14faaeb112a8bbaf5c0cfc73/d1d50d66',
                'titre' => 'GAMME COMMERCIALE',
                'prix' => 
                    '<li>Rotation normale à partir de 1 950 000 XPF TTC
                    </li><li>Contre rotation à partir de 2 050 000 XPF TTC',
                'caracteristique_1' => 
                    '<li>Type : 4 temps, 4 cylindres en ligne, 16 soupapes, DOHC
                    </li><li>Cylindrée : 2670cc
                    </li><li>Bougie : LFR5A-11
                    </li><li>Alésage X Course : 94.0X96.2mm
                    </li><li>Taux de compression : 9.0 :1
                    </li><li>Plage de régime à plein gaz : 5000 – 6000tr/minute
                    </li><li>Puissance maximale : 110.3kw (150cv)
                    </li><li>Système d’alimentation : Injection Electronique EFI
                    </li><li>Lubrification : Carter humide
                    </li><li>Capacité en huile : 4.5 litres
                    </li><li>Allumage : Electronique (CDI)
                    </li><li>Mise en route : Électrique avec prime Start™
                    </li><li>Consommation de carburant : 56,3L/h @5500tr/minute</li>',
                'caracteristique_2' => 
                    '<li>Poids avec hélice : 223kg
                    </li><li>Longueur d’arbre : X-Long(643mm)</li>',
                'caracteristique_3' => 
                    '<li>TRIM : Variable
                    </li><li>Angle de virage : 32 degrés
                    </li><li>Contrôle : Commande à distance
                    </li><li>Sortie de l’alternateur : 35Ah (Charge Batterie 21Ah)</li>',
                ],
                [
                'product_type_id' => 2,
                'name' => 'F200BETX',
                'image' => 'https://dl.airtable.com/.attachmentThumbnails/4275f7d25aca4ddf07a34aaf0241f680/dd9e3ae0',
                'titre' => 'GAMME COMMERCIALE',
                'prix' => 
                    '<li>Rotation normale à partir de 2 200 000 XPF TTC
                    </li><li>Contre  rotation à partir de 2 300 000 XPF TTC',
                'caracteristique_1' => 
                    '<li>Type : 4 temps, 6 cylindres (60°) 24 soupapes en V, DOHC avec VTC
                    </li><li>Cylindrée : 3352cc
                    </li><li>Bougie : LFR6A-11
                    </li><li>Alésage X Course : 94.0X80.5mm
                    </li><li>Taux de compression : 9.9 :1
                    </li><li>Plage de régime à plein gaz : 5000 – 6000tr/minute
                    </li><li>Puissance maximale : 147.1 (200cv)
                    </li><li>Système d’alimentation : Injection Electronique
                    </li><li>Lubrification : Carter humide
                    </li><li>Capacité en huile : 4.7 litres
                    </li><li>Allumage : Electronique (CDI)
                    </li><li>Mise en route : Electrique
                    </li><li>Consommation de carburant : 73,7L/h @5500tr/minute</li>',
                'caracteristique_2' => 
                    '<li>Poids avec hélice : 283 kg
                    </li><li>Longueur d’arbre : 643mm</li>',
                'caracteristique_3' => 
                    '<li>TRIM : Electrique variable
                    </li><li>Angle de virage : 32 degrés
                    </li><li>Contrôle : Commande à distance
                    </li><li>Sortie de l’alternateur : 46Ah (Charge Batterie 27Ah)</li>',
                ],
                [
                'product_type_id' => 2,
                'name' => 'F250HETX',
                'image' => 'https://dl.airtable.com/.attachmentThumbnails/05f217fe8b6e743b6a2a9af510b9a8f4/ba250fe2',
                'titre' => 'GAMME COMMERCIALE',
                'prix' => 
                    '<li>Rotation normale à partir de 2 750 000 XPF TTC
                    </li><li>Contre rotation à partir de 2 850 000 XPF TTC',
                'caracteristique_1' => 
                    '<li>Type : 4 temps, 6 cylindres en V, 24 soupapes, DOHC
                    </li><li>Cylindrée : 3352cc
                    </li><li>Bougie : LFR6A-11
                    </li><li>Alésage X Course : 94.0X80.5mm
                    </li><li>Taux de compression : 9.9 :1
                    </li><li>Plage de régime à plein gaz : 5000 – 6000tr/minute
                    </li><li>Puissance maximale : 183.9 (250cv)
                    </li><li>Système d’alimentation : Injection Electronique EFI
                    </li><li>Lubrification : Carter humide
                    </li><li>Capacité en huile : 4.7 litres
                    </li><li>Allumage : Electronique CDI
                    </li><li>Mise en route : Électrique avec Prime Start™
                    </li><li>Consommation de carburant : 75,6L/h @5500tr/minute</li>',
                'caracteristique_2' => 
                    '<li>Poids avec hélice : 283 kg
                    </li><li>Longueur d’arbre : X-Long(643mm)</li>',
                'caracteristique_3' => 
                    '<li>TRIM : Electrique variable
                    </li><li>Angle de virage : 32 degrés
                    </li><li>Contrôle : Commande à distance
                    </li><li>Sortie de l’alternateur : 46Ah (Charge Batterie 27Ah)</li>',
                ],
                [
                'product_type_id' => 2,
                'name' => 'F300DETX',
                'image' => 'https://dl.airtable.com/.attachmentThumbnails/f22d5947eb5c0f584d296a93c0120b83/81ea7f79',
                'titre' => 'GAMME COMMERCIALE',
                'prix' => 
                    '<li>Rotation normale à partir de 3 500 000 XPF TTC
                    </li><li>Contre rotation à partir de 3 600 000 XPF TTC',
                'caracteristique_1' => 
                    '<li>Type : 4 temps, 6 cylindres en V à 60°, 24 soupapes, DOHC avec VTC
                    </li><li>Cylindrée : 4169cc
                    </li><li>Bougie : LFR6A-11
                    </li><li>Alésage X Course : 96.0X96.0mm
                    </li><li>Taux de compression : 10.3 :1
                    </li><li>Plage de régime à plein gaz : 5000 – 6000tr/minute
                    </li><li>Puissance maximale : 300cv
                    </li><li>Système d’alimentation : Injection Electronique EFI
                    </li><li>Lubrification : Carter humide
                    </li><li>Capacité en huile : 6.3 litres
                    </li><li>Allumage : Electronique CDI
                    </li><li>Mise en route : Électrique avec prime Start™
                    </li><li>Consommation de carburant : 105,0L/h @5500tr/minute</li>',
                'caracteristique_2' => 
                    '<li>Poids avec hélice : 269 kg
                    </li><li>Longueur d’arbre : 642mm</li>',
                'caracteristique_3' => 
                    '<li>TRIM : Electrique variable
                    </li><li>Angle de virage : 32 degrés
                    </li><li>Contrôle : Commande à distance
                    </li><li>Sortie de l’alternateur : 46Ah(Charge Batterie 55Ah)</li>',
                ],
    ];
            foreach($moteur4tCommerciales AS $moteur4tCommerciale):
                Moteur4tCommerciale::create($moteur4tCommerciale);
            endforeach;
    }
}
