<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use App\Models\Moteur2T;

class Moteur2TController extends Controller
{
    public function getAll(){
        $moteurs2t = Moteur2t::all();
        return Response::json($moteurs2t, 200);
    }

    public function getById($id){
        $moteur2t = Moteur2t::find($id);
        return Response::json($moteur2t, 200);
    }

    public function create(Request $request){

        $moteur2t = new Moteur2t;
        $moteur2t->moteur2t_id = $request->moteur2t_id;
        $moteur2t->product_type_id = $request->product_type_id;
        $moteur2t->name = $request->name;
        $moteur2t->image = $request->image;
        $moteur2t->titre = $request->titre;
        $moteur2t->titre = $request->titre_description;
        $moteur2t->description = $request->description;
        $moteur2t->prix = $request->prix;
        $moteur2t->caracteristique_1 = $request->caracteristique_1;
        $moteur2t->caracteristique_2 = $request->caracteristique_2;
        $moteur2t->caracteristique_3 = $request->caracteristique_3;
        $moteur2t->save();

        return Response::json($moteur2t, 200);
    }

    public function update($id, Request $request){
        $moteur2t = Moteur2t::find($id);
        
        $moteur2t->moteur2t_id = $request->moteur2t_id;
        $moteur2t->product_type_id = $request->product_type_id;
        $moteur2t->name = $request->name;
        $moteur2t->image = $request->image;
        $moteur2t->titre = $request->titre;
        $moteur2t->titre = $request->titre_description;
        $moteur2t->description = $request->description;
        $moteur2t->prix = $request->prix;
        $moteur2t->caracteristique_1 = $request->caracteristique_1;
        $moteur2t->caracteristique_2 = $request->caracteristique_2;
        $moteur2t->caracteristique_3 = $request->caracteristique_3;
        $moteur2t->save();

        return Response::json($moteur2t, 200);
    }

    public function delete($id){
        Moteur2t::destroy($id);
        return Response::json("record deleted", 200);
    }
}
