<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use App\Models\Moto;

class MotoController extends Controller
{
    public function getAll(){
        $motos = Moto::all();
        return Response::json($motos, 200);
    }

    public function getById($id){
        $moto = Moto::find($id);
        return Response::json($moto, 200);
    }

    public function create(Request $request){

        $moto = new Moto;
        $moto->moto_id = $request->moto_id;
        $moto->product_type_id = $request->product_type_id;
        $moto->name = $request->name;
        $moto->video = $request->video;
        $moto->image = $request->image;
        $moto->titre = $request->titre;
        $moto->petit_titre = $request->petit_titre;
        $moto->description = $request->description;
        $moto->prix = $request->prix;
        $moto->image_force1 = $request->image_force1;
        $moto->force1 = $request->force1;
        $moto->image_force2 = $request->image_force2;
        $moto->force2 = $request->force2;
        $moto->image_force3 = $request->image_force3;
        $moto->force3 = $request->force3;
        $moto->image_force4 = $request->image_force4;
        $moto->force4 = $request->force4;
        $moto->image_force5 = $request->image_force5;
        $moto->force5 = $request->force5;
        $moto->caracteristique_1 = $request->caracteristique_1;
        $moto->caracteristique_2 = $request->caracteristique_2;
        $moto->caracteristique_3 = $request->caracteristique_3;
        $moto->save();

        return Response::json($moto, 200);
    }

    public function update($id, Request $request){
        $moto = Moto::find($id);
        
        $moto->moto_id = $request->moto_id;
        $moto->product_type_id = $request->product_type_id;
        $moto->name = $request->name;
        $moto->video = $request->video;
        $moto->image = $request->image;
        $moto->titre = $request->titre;
        $moto->petit_titre = $request->petit_titre;
        $moto->description = $request->description;
        $moto->prix = $request->prix;
        $moto->force = $request->force;
        $moto->image_force1 = $request->image_force1;
        $moto->force1 = $request->force1;
        $moto->image_force2 = $request->image_force2;
        $moto->force2 = $request->force2;
        $moto->image_force3 = $request->image_force3;
        $moto->force3 = $request->force3;
        $moto->image_force4 = $request->image_force4;
        $moto->force4 = $request->force4;
        $moto->image_force5 = $request->image_force5;
        $moto->caracteristique_1 = $request->caracteristique_1;
        $moto->caracteristique_2 = $request->caracteristique_2;
        $moto->caracteristique_3 = $request->caracteristique_3;
        $moto->save();

        return Response::json($moto, 200);
    }

    public function delete($id){
        Moto::destroy($id);
        return Response::json("record deleted", 200);
    }
}
