<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use App\Models\Team;

class TeamController extends Controller
{
    public function getAll(){
        $teams = Team::all();
        return Response::json($teams, 200);
    }

    public function getById($id){
        $team = Team::find($id);
        return Response::json($team, 200);
    }

    public function create(Request $request){

        $team = new Team;
        $team->team_id = $request->team_id;
        $team->firstname = $request->firstname;
        $team->lastname = $request->lastname;
        $team->function = $request->function;
        $team->image = $request->image;
        $team->save();

        return Response::json($team, 200);
    }

    public function update($id, Request $request){
        $team = Team::find($id);
        
        $team->team_id = $request->team_id;
        $team->firstname = $request->firstname;
        $team->lastname = $request->lastname;
        $team->function = $request->function;
        $team->image = $request->image;
        $team->save();

        return Response::json($team, 200);
    }

    public function delete($id){
        Team::destroy($id);
        return Response::json("record deleted", 200);
    }
}
