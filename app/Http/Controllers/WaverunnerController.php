<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use App\Models\Waverunner;

class WaverunnerController extends Controller
{
    public function getAll(){
        $waverunners = Waverunner::all();
        return Response::json($waverunners, 200);
    }

    public function getById($id){
        $waverunner = Waverunner::find($id);
        return Response::json($waverunner, 200);
    }

    public function create(Request $request){

        $waverunner = new Waverunner;
        $waverunner->user_id = $request->user_id;
        $waverunner->waverunner_id = $request->waverunner_id;
        $waverunner->save();

        return Response::json($waverunner, 200);
    }

    public function update($id, Request $request){
        $waverunner = Waverunner::find($id);
        
        $waverunner->user_id = $request->user_id;
        $waverunner->waverunner_id = $request->waverunner_id;
        $waverunner->save();

        return Response::json($waverunner, 200);
    }

    public function delete($id){
        Waverunner::destroy($id);
        return Response::json("record deleted", 200);
    }
}
