<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use App\Models\Moteur4tCommerciale;

class Moteur4tCommercialeController extends Controller
{
    public function getAll(){
        $moteur4tCommerciales = Moteur4tCommerciale::all();
        return Response::json($moteur4tCommerciales, 200);
    }

    public function getById($id){
        $moteur4tCommerciale = Moteur4tCommerciale::find($id);
        return Response::json($moteur4tCommerciale, 200);
    }

    public function create(Request $request){

        $moteur4tCommerciale = new Moteur4tCommerciale;
        $moteur4tCommerciale->moteur4t_commerciale_id = $request->moteur4t_commerciale_id;
        $moteur4tCommerciale->product_type_id = $request->product_type_id;
        $moteur4tCommerciale->name = $request->name;
        $moteur4tCommerciale->image = $request->image;
        $moteur4tCommerciale->titre = $request->titre;
        $moteur4tCommerciale->prix = $request->prix;
        $moteur4tCommerciale->caracteristique_1 = $request->caracteristique_1;
        $moteur4tCommerciale->caracteristique_2 = $request->caracteristique_2;
        $moteur4tCommerciale->caracteristique_3 = $request->caracteristique_3;
        $moteur4tCommerciale->save();

        return Response::json($moteur4tCommerciale, 200);
    }

    public function update($id, Request $request){
        $moteur4tCommerciale = Moteur4tCommerciale::find($id);
        
        $moteur4tCommerciale->moteur4t_commerciale_id = $request->moteur4t_commerciale_id;
        $moteur4tCommerciale->product_type_id = $request->product_type_id;
        $moteur4tCommerciale->name = $request->name;
        $moteur4tCommerciale->image = $request->image;
        $moteur4tCommerciale->titre = $request->titre;
        $moteur4tCommerciale->prix = $request->prix;
        $moteur4tCommerciale->caracteristique_1 = $request->caracteristique_1;
        $moteur4tCommerciale->caracteristique_2 = $request->caracteristique_2;
        $moteur4tCommerciale->caracteristique_3 = $request->caracteristique_3;
        $moteur4tCommerciale->save();

        return Response::json($moteur4tCommerciale, 200);
    }

    public function delete($id){
        Moteur4tCommerciale::destroy($id);
        return Response::json("record deleted", 200);
    }
}
