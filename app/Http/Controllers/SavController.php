<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use App\Models\Sav;

class SavController extends Controller
{
    public function getAll(){
        $sav = Sav::all();
        return Response::json($sav, 200);
    }

    public function getById($id) {
        if ($id == null) {return "Error: 'id' missing";}
        return Response::json(Sav::find($id), 200);
    }
}
