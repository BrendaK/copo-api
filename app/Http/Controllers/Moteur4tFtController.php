<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use App\Models\Moteur4tFt;
use App\Models\Moteur4T;
use App\Models\Moteur4tCommerciale;


class Moteur4tFtController extends Controller
{
    public function getAll(){
        $moteurs4tFts = Moteur4tFt::all();
        return Response::json($moteurs4tFts, 200);
    }

    public function getById($id){
        $moteur4tFt = Moteur4tFt::find($id)
        ->join('Moteur4t', 'Moteur4t.moteur4t_id', '=' , 'MoteurCommerciale.moteur4t_commerciale_id')
        ->join('MoteurCommerciale.moteur4t_commerciale_id', '=' ,'Moteur4t.moteur4t_id')
        ->select('Moteur4t.name', 'Moteur4tFT.name', 'MoteurCommerciale.name')
        ->get();
        return Response::json($moteur4tFt, 200);
    }

    public function create(Request $request){

        $moteur4tFt = new Moteur4tFt;
        $moteur4tFt->moteur4t_ft_id = $request->moteur4t_ft_id;
        $moteur4tFt->product_type_id = $request->product_type_id;
        $moteur4tFt->name = $request->name;
        $moteur4tFt->image = $request->image;
        $moteur4tFt->titre = $request->titre;
        $moteur4tFt->description = $request->description;
        $moteur4tFt->prix = $request->prix;
        $moteur4tFt->caracteristique_1 = $request->caracteristique_1;
        $moteur4tFt->caracteristique_2 = $request->caracteristique_2;
        $moteur4tFt->caracteristique_3 = $request->caracteristique_3;
        $moteur4tFt->save();

        return Response::json($moteur4tFt, 200);
    }

    public function update($id, Request $request){
        $moteur4tFt = Moteur4tFt::find($id);
        
        $moteur4tFt->moteur4t_ft_id = $request->moteur4t_ft_id;
        $moteur4tFt->product_type_id = $request->product_type_id;
        $moteur4tFt->name = $request->name;
        $moteur4tFt->image = $request->image;
        $moteur4tFt->titre = $request->titre;
        $moteur4tFt->description = $request->description;
        $moteur4tFt->prix = $request->prix;
        $moteur4tFt->caracteristique_1 = $request->caracteristique_1;
        $moteur4tFt->caracteristique_2 = $request->caracteristique_2;
        $moteur4tFt->caracteristique_3 = $request->caracteristique_3;
        $moteur4tFt->save();

        return Response::json($moteur4tFt, 200);
    }

    public function delete($id){
        Moteur4tFt::destroy($id);
        return Response::json("record deleted", 200);
    }
}
