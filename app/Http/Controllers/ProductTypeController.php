<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use App\Models\ProductType;

class ProductTypeController extends Controller
{
    public function getAll(){
        $productTypes = ProductType::all();
        return Response::json($productTypes, 200);
    }

    public function getById($id){
        $productType = ProductType::find($id);
        return Response::json($productType, 200);
    }

    public function create(Request $request){

        $productType = new ProductType;
        $productType->category_id = $request->category_id;
        $productType->name = $request->name;
        $productType->save();

        return Response::json($productType, 200);
    }

    public function update($id, Request $request){
        $productType = ProductType::find($id);
        
        $productType->category_id = $request->category_id;
        $productType->name = $request->name;
        $productType->save();

        return Response::json($productType, 200);
    }

    public function delete($id){
        ProductType::destroy($id);
        return Response::json("record deleted", 200);
    }
}
