<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use App\Models\Product;

class ProductController extends Controller
{
    public function getAll(){
        $products = Product::all();
        return Response::json($products, 200);
    }

    public function getById($id){
        $product = Product::find($id);
        return Response::json($product, 200);
    }

    public function create(Request $request){

        $product = new Product;
        $product->productType_id = $request->productType_id;
        $product->name = $request->name;
        $product->vidéo = $request->vidéo;
        $product->image = $request->image;
        $product->titre = $request->titre;
        $product->petit_titre = $request->petit_titre;
        $product->description = $request->description;
        $product->prix = $request->prix;
        $product->image_force1 = $request->image_force1;
        $product->force1 = $request->force1;
        $product->image_force2 = $request->image_force2;
        $product->force2 = $request->force2;
        $product->image_force3 = $request->image_force3;
        $product->force3 = $request->force3;
        $product->image_force4 = $request->image_force4;
        $product->force4 = $request->force4;
        $product->image_force5 = $request->image_force5;
        $product->force5 = $request->force5;
        $product->caractéristique = $request->caractéristique;
        $product->save();

        return Response::json($product, 200);
    }

    public function update($id, Request $request){
        $product = Product::find($id);
        
        $product->productType_id = $request->productType_id;
        $product->name = $request->name;
        $product->vidéo = $request->vidéo;
        $product->image = $request->image;
        $product->titre = $request->titre;
        $product->petit_titre = $request->petit_titre;
        $product->description = $request->description;
        $product->prix = $request->prix;
        $product->image_force1 = $request->image_force1;
        $product->force1 = $request->force1;
        $product->image_force2 = $request->image_force2;
        $product->force2 = $request->force2;
        $product->image_force3 = $request->image_force3;
        $product->force3 = $request->force3;
        $product->image_force4 = $request->image_force4;
        $product->force4 = $request->force4;
        $product->image_force5 = $request->image_force5;
        $product->force5 = $request->force5;
        $product->caractéristique = $request->caractéristique;
        $product->save();

        return Response::json($product, 200);
    }

    public function delete($id){
        Product::destroy($id);
        return Response::json("record deleted", 200);
    }
}
