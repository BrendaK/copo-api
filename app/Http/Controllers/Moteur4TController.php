<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use App\Models\Moteur4T;

class Moteur4TController extends Controller
{
    public function getAll(){
        $moteur4ts = Moteur4t::all();
        return Response::json($moteur4ts, 200);
    }

    public function getById($id){
        $moteur4t = Moteur4t::find($id);
        return Response::json($moteur4t, 200);
    }

    public function create(Request $request){

        $moteur4t = new Moteur4t;
        $moteur4t->moteur4t_id = $request->moteur4t_id;
        $moteur4t->product_type_id = $request->product_type_id;
        $moteur4t->name = $request->name;
        $moteur4t->image = $request->image;
        $moteur4t->titre = $request->titre;
        $moteur4t->petit_titre = $request->petit_titre;
        $moteur4t->description = $request->description;
        $moteur4t->prix = $request->prix;
        $moteur4t->caracteristique_1 = $request->caracteristique_1;
        $moteur4t->caracteristique_2 = $request->caracteristique_2;
        $moteur4t->caracteristique_3 = $request->caracteristique_3;
        $moteur4t->save();

        return Response::json($moteur4t, 200);
    }

    public function update($id, Request $request){
        $moteur4t = Moteur4t::find($id);
        
        $moteur4t->moteur4t_id = $request->moteur4t_id;
        $moteur4t->product_type_id = $request->product_type_id;
        $moteur4t->name = $request->name;
        $moteur4t->image = $request->image;
        $moteur4t->titre = $request->titre;
        $moteur4t->petit_titre = $request->petit_titre;
        $moteur4t->description = $request->description;
        $moteur4t->prix = $request->prix;
        $moteur4t->caracteristique_1 = $request->caracteristique_1;
        $moteur4t->caracteristique_2 = $request->caracteristique_2;
        $moteur4t->caracteristique_3 = $request->caracteristique_3;
        $moteur4t->save();

        return Response::json($moteur4t, 200);
    }

    public function delete($id){
        Moteur4t::destroy($id);
        return Response::json("record deleted", 200);
    }
}
