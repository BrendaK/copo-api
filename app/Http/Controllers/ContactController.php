<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use App\Models\Contact;

class ContactController extends Controller
{
    public function create(Request $request) {
        $contact = new Contact;
        $contact->email = $request->email;
        $contact->message = $request->message;
        $contact->save();
        return Response::json("Send", 200);
    }
}
