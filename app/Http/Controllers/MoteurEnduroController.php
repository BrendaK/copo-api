<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use App\Models\MoteurEnduro;

class MoteurEnduroController extends Controller
{
    public function getAll(){
        $moteursEnduro = MoteurEnduro::all();
        return Response::json($moteursEnduro, 200);
    }

    public function getById($id){
        $moteurEnduro = MoteurEnduro::find($id);
        return Response::json($moteurEnduro, 200);
    }

    public function create(Request $request){

        $moteurEnduro = new MoteurEnduro;
        $moteurEnduro->moteurEnduro_id = $request->moteurEnduro_id;
        $moteurEnduro->product_type_id = $request->product_type_id;
        $moteurEnduro->name = $request->name;
        $moteurEnduro->image = $request->image;
        $moteurEnduro->titre = $request->titre;
        $moteurEnduro->titre = $request->titre_description;
        $moteurEnduro->description = $request->description;
        $moteurEnduro->prix = $request->prix;
        $moteurEnduro->caracteristique_1 = $request->caracteristique_1;
        $moteurEnduro->caracteristique_2 = $request->caracteristique_2;
        $moteurEnduro->caracteristique_3 = $request->caracteristique_3;
        $moteurEnduro->save();

        return Response::json($moteurEnduro, 200);
    }

    public function update($id, Request $request){
        $moteurEnduro = MoteurEnduro::find($id);
        
        $moteurEnduro->moteurEnduro_id = $request->moteurEnduro_id;
        $moteurEnduro->product_type_id = $request->product_type_id;
        $moteurEnduro->name = $request->name;
        $moteurEnduro->image = $request->image;
        $moteurEnduro->titre = $request->titre;
        $moteurEnduro->titre = $request->titre_description;
        $moteurEnduro->description = $request->description;
        $moteurEnduro->prix = $request->prix;
        $moteurEnduro->caracteristique_1 = $request->caracteristique_1;
        $moteurEnduro->caracteristique_2 = $request->caracteristique_2;
        $moteurEnduro->caracteristique_3 = $request->caracteristique_3;
        $moteurEnduro->save();

        return Response::json($moteurEnduro, 200);
    }

    public function delete($id){
        MoteurEnduro::destroy($id);
        return Response::json("record deleted", 200);
    }
}
