<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use App\Notifications\EmailVerifyAt;
use App\Notifications\ResetPassword;

class UserController extends Controller
{
    public function getAll (){
        $users = User::all();
        return Response::json($users, 200);
    }

    public function getAllClient (){
        $users = User::where("user_type_id", 2)->get();
        return Response::json($users, 200);
    }

    public function getById ($id){
        $user = User::with(["userTypes"])->find($id);
        return Response::json($user, 200);
    }

    public function create (Request $request){
        $user = new User;
        $user->lastname = $request->lastname;
        $user->firstname = $request->firstname;
        $user->identifiant = $request->identifiant;
        $user->email = $request->email;
        $user->password = Hash::make($request->password);
        $user->save();
        return Response::json($user, 200);
    }

    public function resetPassword ($id){
        $user = User::find($id);
        // $user = User::new($user, $request);
        $user->email_verified_at = now();

        $user->save();

        return Response::json($user, 200);
    }

    public function addUserVip(Request $request) {
        $user = User::find($request->id);
        $user->check_vip = $request->check_vip;
        $user->save();
        return Response::json($user, 200);
    }
    
    // public function getPosition($id) {
    //     $user = User::find($id);
    //     return Response::json()
    // }

    public function update ($id, Request $request){
        $user = User::find($id);
        $user = User::new($user, $request);
        return Response::json($user, 200);
    }

    public function delete ($id){
        User::destroy($id);
        return Response::json("record deleted", 200);
    }

    public function getByResearchUser(Request $request) {
        $users = User::where("firstname", "LIKE", "%" . $request->q . "%")
        ->orWhere("lastname","LIKE", "%" . $request->q . "%")
        ->get();
        return Response::json($users, 200);
    }
    
    public function login(Request $request){
        $identifiant = $request->pseudo;
        $password = $request->password;
        if(Auth::attempt(['identifiant' => $identifiant, 'password' => $password])){
            $user = Auth::user();
            $success['token'] =  $user->createToken('Laravel')->accessToken; 
            $success['lastname'] =  $user->lastname;
            $success['firstname'] =  $user->firstname;
            $success['email'] =  $user->email;
            $success['code_generate'] =  $user->code_generate;
            $success['code_generate_verified'] =  $user->code_generate_verified;
            $success['user_type_id'] =  $user->user_type_id;
            $success['id'] =  $user->id;
            return response()->json($success, 200); 
        } else { 
            return response()->json(['error'=>'Unauthorised'], 401); 
        }
    }

    public function logout() {
        if(Auth::check()) {
            Auth::user()->token()->delete();
        }
        return Response::json("vous avez été déconnecté", 200);
    }
}
