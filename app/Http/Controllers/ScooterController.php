<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use App\Models\Scooter;

class ScooterController extends Controller
{
    public function getAll(){
        $scooters = Scooter::all();
        return Response::json($scooters, 200);
    }

    public function getById($id){
        $scooter = Scooter::find($id);
        return Response::json($scooter, 200);
    }

    public function create(Request $request){

        $scooter = new Scooter;
        $scooter->scooter_id = $request->scooter_id;
        $scooter->product_type_id = $request->product_type_id;
        $scooter->name = $request->name;
        $scooter->image = $request->image;
        $scooter->video = $request->video;
        $scooter->titre = $request->titre;
        $scooter->petit_titre = $request->petit_titre;
        $scooter->description = $request->description;
        $scooter->prix = $request->prix;
        $scooter->image_force1 = $request->image_force1;
        $scooter->force1 = $request->force1;
        $scooter->image_force2 = $request->image_force2;
        $scooter->force2 = $request->force2;
        $scooter->image_force3 = $request->image_force3;
        $scooter->force3 = $request->force3;
        $scooter->image_force4 = $request->image_force4;
        $scooter->force4 = $request->force4;
        $scooter->image_force5 = $request->image_force5;
        $scooter->force5 = $request->force5;
        $scooter->caracteristique_1 = $request->caracteristique_1;
        $scooter->caracteristique_2 = $request->caracteristique_2;
        $scooter->caracteristique_3 = $request->caracteristique_3;
        $scooter->save();

        return Response::json($scooter, 200);
    }

    public function update($id, Request $request){
        $scooter = Scooter::find($id);
        
        $scooter->scooter_id = $request->scooter_id;
        $scooter->product_type_id = $request->product_type_id;
        $scooter->name = $request->name;
        $scooter->image = $request->image;
        $scooter->video = $request->video;
        $scooter->titre = $request->titre;
        $scooter->petit_titre = $request->petit_titre;
        $scooter->description = $request->description;
        $scooter->prix = $request->prix;
        $scooter->image_force1 = $request->image_force1;
        $scooter->force1 = $request->force1;
        $scooter->image_force2 = $request->image_force2;
        $scooter->force2 = $request->force2;
        $scooter->image_force3 = $request->image_force3;
        $scooter->force3 = $request->force3;
        $scooter->image_force4 = $request->image_force4;
        $scooter->force4 = $request->force4;
        $scooter->image_force5 = $request->image_force5;
        $scooter->force5 = $request->force5;
        $scooter->caracteristique_1 = $request->caracteristique_1;
        $scooter->caracteristique_2 = $request->caracteristique_2;
        $scooter->caracteristique_3 = $request->caracteristique_3;
        $scooter->save();

        return Response::json($scooter, 200);
    }

    public function delete($id){
        Scooter::destroy($id);
        return Response::json("record deleted", 200);
    }
}
