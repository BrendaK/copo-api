<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Moteur4T extends Model
{
    use HasFactory;

    public function user() {
        return $this->hasMany('App\Models\User', 'user_id');
    }
}
